package Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ntamtech.wayti.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import Bean.historyBean;
import Bean.requests;
import Singleton.Singleton;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Ayaomar on 5/2/2017.
 */

public class requestAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    ArrayList<requests> requests;
    private DatabaseReference mDatabase;


    public requestAdapter(Context context, ArrayList<requests> requests) {
        this.context = context;
        this.requests = requests;
        inflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return requests.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("language", MODE_PRIVATE);
        String lang = sharedPreferences.getString("lang", "");
        if (lang.equals("ar")){
            view = inflater.inflate(R.layout.custom_request_cell, null);
        }else {
            view = inflater.inflate(R.layout.custom_request_cell_left, null);
        }

        TextView nameTV = (TextView) view.findViewById(R.id.name);
        Button accept = (Button) view.findViewById(R.id.accept);
        final Button reject = (Button) view.findViewById(R.id.reject);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        ImageView profileImgTV = (ImageView)view.findViewById(R.id.userImg);

        nameTV.setText(requests.get(i).getUsername());
        Glide.with(context).load(requests.get(i).getUser_photo()).into(profileImgTV);

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences preferences = context.getSharedPreferences("userData", 0);
                final String userId = preferences.getString("id", "");
                final String phone = preferences.getString("phone", "");
                final String driverPhoto = preferences.getString("profileImg", "");
                final String name = preferences.getString("name", "");
                final String lat = preferences.getString("lat", "");
                final String lng = preferences.getString("lng", "");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                String currentDateandTime = sdf.format(new Date());

                Map<String, Object> acceptData = new HashMap<String, Object>();
                acceptData.put("status", 0);
                acceptData.put("driver_name", name);
                acceptData.put("driver_phone", phone);
                acceptData.put("driver_photo", driverPhoto);
                acceptData.put("user_photo", requests.get(i).getUser_photo());
                acceptData.put("datetime", currentDateandTime);
                acceptData.put("user_phone", requests.get(i).getUser_phone());
                acceptData.put("username", requests.get(i).getUsername());
                acceptData.put("userID",requests.get(i).getToId());
                acceptData.put("user_latitude",requests.get(i).getUser_latitude());
                acceptData.put("user_longitude",requests.get(i).getUser_longitude());
                acceptData.put("driver_latitude",Double.parseDouble(lat));
                acceptData.put("driver_longitude",Double.parseDouble(lng));
                acceptData.put("driverID",userId);
                String uuid = UUID.randomUUID().toString();

                SharedPreferences.Editor editor = context.getSharedPreferences("requestData", MODE_PRIVATE).edit();
                editor.putString("user_latitude", requests.get(i).getUser_latitude()+"");
                editor.putString("user_longitude", requests.get(i).getUser_longitude()+"");
                editor.putString("key",userId+"-"+uuid+"-"+requests.get(i).getToId());
                editor.commit();

                mDatabase.child("request").child("accepted").child(userId+"-"+uuid+"-"+requests.get(i).getToId()).setValue(acceptData);
                mDatabase.child("request").child("pending").child(userId+"-"+requests.get(i).getToId()).removeValue();
                requests.remove(i);
//                Singleton.getInstance().getRequests().remove(i);
                notifyDataSetChanged();
                ((Activity)context).finish();

            }
        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences preferences = context.getSharedPreferences("userData", 0);
                final String userId = preferences.getString("id", "");
                final String phone = preferences.getString("phone", "");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                String currentDateandTime = sdf.format(new Date());

                Map<String, Object> data = new HashMap<String, Object>();
                data.put("userID",requests.get(i).getToId());
                data.put("driverID",userId);
                data.put("reason","");
                data.put("datetime",currentDateandTime);
                data.put("isRejected", 1);
                String uuid = UUID.randomUUID().toString();
                mDatabase.child("request").child("failed").child(userId+"-"+uuid+"-"+requests.get(i).getToId()).setValue(data);
                mDatabase.child("request").child("pending").child(userId+"-"+requests.get(i).getToId()).removeValue();
                requests.remove(i);
                notifyDataSetChanged();

                SharedPreferences.Editor editor = context.getSharedPreferences("requestData", MODE_PRIVATE).edit();
                editor.putString("user_latitude"," ");
                editor.putString("user_longitude", " ");
                editor.putString("key"," ");
                editor.commit();


            }
        });
        return view;
    }


}