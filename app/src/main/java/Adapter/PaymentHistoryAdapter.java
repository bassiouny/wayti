package Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ntamtech.wayti.R;

import java.util.ArrayList;

import Bean.PaymentHistoryBean;
import Bean.historyBean;

/**
 * Created by Ayaomar on 5/2/2017.
 */

public class PaymentHistoryAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    ArrayList<PaymentHistoryBean> paymentHistoryBeen;


    public PaymentHistoryAdapter(Context context, ArrayList<PaymentHistoryBean> paymentHistoryBeen) {
        this.context = context;
        this.paymentHistoryBeen = paymentHistoryBeen;
        inflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return paymentHistoryBeen.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.commission_history_cell, null);
        TextView bankNameTV = (TextView) view.findViewById(R.id.bankName);
        TextView numTV = (TextView) view.findViewById(R.id.num);
        TextView dateTV = (TextView) view.findViewById(R.id.date);
        TextView priceTV = (TextView)view.findViewById(R.id.price);

        bankNameTV.setText(paymentHistoryBeen.get(i).getBank_name());
        numTV.setText(paymentHistoryBeen.get(i).getPayment_no());
        priceTV.setText("تم دفع"+" "+paymentHistoryBeen.get(i).getPrice()+" "+"ريال" );
        dateTV.setText(paymentHistoryBeen.get(i).getDate_time());

        return view;
    }
}