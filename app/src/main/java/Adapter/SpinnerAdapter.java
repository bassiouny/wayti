package Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ntamtech.wayti.R;

/**
 * Created by Ayaom on 5/30/2017.
 */

public class SpinnerAdapter extends ArrayAdapter {
    Context context;
    String[] objects;

    public SpinnerAdapter(Context context, int textViewResourceId, String[] objects) {
        super(context, textViewResourceId, objects);

        this.context = context;
        this.objects = objects;
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater =((Activity) context).getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_spinner, parent, false);

        TextView textView = (TextView)layout.findViewById(R.id.text);
        textView.setText(objects[position]);

//        if (position==(objects.length-1)){
//
//            View view = layout.findViewById(R.id.view);
//            view.setVisibility(View.GONE);
//        }

        return layout;
    }

    // It gets a View that displays in the drop down popup the data at the specified position
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // It gets a View that displays the data at the specified position
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
}