package Singleton;

import java.util.ArrayList;

import Bean.PaymentHistoryBean;
import Bean.requests;

/**
 * Created by Ayaomar on 5/28/2017.
 */

public class Singleton {


    private static Singleton mInstance = null;

    private String mString;
    private  ArrayList<PaymentHistoryBean> historyBeens;
    private ArrayList<Bean.requests> requests;

    private Singleton(){
        mString = "Hello";
        historyBeens = null;
        requests = null;
    }

    public static Singleton getInstance(){
        if(mInstance == null)
        {
            mInstance = new Singleton();
        }
        return mInstance;
    }

    public ArrayList<Bean.requests> getRequests() {
        return requests;
    }

    public void setRequests(ArrayList<Bean.requests> requests) {
        this.requests = requests;
    }

    public String getString(){
        return this.mString;
    }

    public void setString(String value){
        mString = value;
    }

    public ArrayList<PaymentHistoryBean> getHistoryBeens() {
        return historyBeens;
    }

    public void setHistoryBeens(ArrayList<PaymentHistoryBean> historyBeens) {
        this.historyBeens = historyBeens;
    }
}
