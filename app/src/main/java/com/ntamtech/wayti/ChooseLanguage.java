package com.ntamtech.wayti;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Locale;

import config.LANG;

public class ChooseLanguage extends AppCompatActivity {

    Helper helper;
    TextView chooseTV;
    TextView arabicTV;
    TextView userTV;
    Typeface tf;
    RelativeLayout arabicLanguage;
    RelativeLayout englishLanguage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = new Helper();
        helper.hideStatusBar(this);
        setContentView(R.layout.activity_choose_language);
        intial();
        events();


    }

    public void intial() {

        chooseTV = (TextView)findViewById(R.id.choose);
        arabicTV = (TextView)findViewById(R.id.arabic);
        userTV = (TextView)findViewById(R.id.user);
        arabicLanguage = (RelativeLayout)findViewById(R.id.arabicLanguage);
        englishLanguage = (RelativeLayout)findViewById(R.id.english);

        tf = Typeface.createFromAsset(getAssets(),"GE_SS_Text_Light.otf");
        chooseTV.setTypeface(tf);
        arabicTV.setTypeface(tf);
        userTV.setTypeface(tf);

    }

    public void events() {

        arabicLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetLanguage(getApplicationContext(), LANG.ARABIC);
                final Intent mainIntent = new Intent(ChooseLanguage.this, Login.class);
                startActivity(mainIntent);
                finish();
                SharedPreferences.Editor editor = getSharedPreferences("language", MODE_PRIVATE).edit();
                editor.putString("lang", "ar");
                editor.commit();
            }
        });

        englishLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetLanguage(getApplicationContext(), LANG.ENGLISH);
                final Intent mainIntent = new Intent(ChooseLanguage.this, Login.class);
                startActivity(mainIntent);
                finish();

                SharedPreferences.Editor editor = getSharedPreferences("language", MODE_PRIVATE).edit();
                editor.putString("lang", "en");
                editor.commit();
            }
        });

        userTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://play.google.com/store/apps/details?id=com.ntamtech.waytiuser";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

    }

    public static void SetLanguage(Context ctx, LANG selectedLang) {
        if (selectedLang == LANG.ENGLISH) {
            // Change layout to en
            Locale mLocale = new Locale("en");
            Locale.setDefault(mLocale);
            Configuration config = ctx.getResources().getConfiguration();

            if (!config.locale.equals(mLocale)) {
                config.locale = mLocale;
                ctx.getResources().updateConfiguration(config, null);
            }
//            SharedPrefCustom.saveLocalization(ctx, english);
        } else {
            // Change layout to ar
            Locale mLocale = new Locale("ar");
            Locale.setDefault(mLocale);
            Configuration config = ctx.getResources().getConfiguration();

            if (!config.locale.equals(mLocale)) {
                config.locale = mLocale;
                ctx.getResources().updateConfiguration(config, null);
            }
//            SharedPrefCustom.saveLocalization(ctx, arabic);
        }
    }
}
