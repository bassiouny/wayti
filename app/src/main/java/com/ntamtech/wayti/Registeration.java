package com.ntamtech.wayti;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.ntamtech.wayti.Activities.NextRegister;
import com.ntamtech.wayti.Activities.SetLocation;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

import Util.Utility;
import config.LANG;

public class Registeration extends AppCompatActivity {

    Button nextBtn;
    ImageView backIV;
    ImageView profileIV;
    EditText nameET;
    EditText phoneET;
    EditText passwordET;
    String userChoosenTask;
    private int REQUEST_CAMERA = 0;
    private int SELECT_FILE = 1;
    private CoordinatorLayout coordinatorLayout;
    Uri filePath = Uri.parse("");
    StorageReference storageRef;
    String imgName;
    Bitmap bitmap;
    private FirebaseAuth auth;
    private ProgressDialog dialog;

    Spinner citySP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeration);
        intail();
        events();

    }


    public void intail() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        nextBtn = (Button) findViewById(R.id.next);
        backIV = (ImageView) findViewById(R.id.back);
        profileIV = (ImageView) findViewById(R.id.profileIV);
        nameET = (EditText) findViewById(R.id.name);
        phoneET = (EditText) findViewById(R.id.phone);
        //  placeET = (EditText) findViewById(R.id.place);
        citySP = (Spinner) findViewById(R.id.city);
        passwordET = (EditText) findViewById(R.id.password);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        storageRef = FirebaseStorage.getInstance().getReference().child("/images");
        auth = FirebaseAuth.getInstance();
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
    }

    public void events() {
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
                final String name = nameET.getText().toString().trim();
                final String phone = phoneET.getText().toString().trim();
                //     final String place = placeET.getText().toString().trim();
                final String password = passwordET.getText().toString().trim();

                if (Utility.isNotNull(filePath.toString())) {
                    SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                    editor.putString("profileImg", " ");
                    editor.commit();
                }
                if (phone.length() == 9) {
                    if (Utility.isNotNull(name) && Utility.isNotNull(phone) && Utility.isNotNull(password)) {

                        if (password.length() < 6) {
                            Snackbar snackbar = Snackbar.make(coordinatorLayout, "password length must be over 6", Snackbar.LENGTH_SHORT);
                            snackbar.show();
                            dialog.dismiss();
                        } else {
                            auth.createUserWithEmailAndPassword(phone + "@gmail.com", password)
                                    .addOnCompleteListener(Registeration.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            //    Toast.makeText(Registeration.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                                            if (!task.isSuccessful()) {
                                                dialog.hide();
                                                String msg = task.getException().getMessage().replace("email", "phone");
                                                String message = msg.replace("address", "");
                                                Toast.makeText(Registeration.this, message, Toast.LENGTH_SHORT).show();
                                            } else {

                                                imgName = task.getResult().getUser().getUid();
                                                SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                                                editor.putString("id", task.getResult().getUser().getUid());
                                                editor.putString("name", name);
                                                editor.putString("phone", phone);
                                                editor.putString("city", citySP.getSelectedItem().toString());
                                                editor.putString("password", password);
                                                editor.commit();
                                                if (Utility.isNotNull(filePath.toString())) {
                                                    addData(filePath);
                                                } else {
                                                    dialog.dismiss();
                                                    final Intent mainIntent = new Intent(Registeration.this, NextRegister.class);
                                                    startActivity(mainIntent);
                                                }
                                            }
                                        }
                                    });
                        }
                    } else {
                        dialog.hide();
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.fillData), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.hide();
                    dialog.hide();
                    final AlertDialog alertDialog = new AlertDialog.Builder(Registeration.this).create();
                    alertDialog.setMessage(getString(R.string.format_num));
                    alertDialog.setCancelable(false);
                    alertDialog.setButton(Dialog.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }
            }
        });
//        placeET.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final Intent mainIntent = new Intent(Registeration.this, SetLocation.class);
//                startActivity(mainIntent);
//            }
//        });
        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        profileIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
    }

    private void selectImage() {
        final CharSequence[] items = {(getString(R.string.take_photo)), (getString(R.string.choose_from_library)), (getString(R.string.cancel))};
        AlertDialog.Builder builder = new AlertDialog.Builder(Registeration.this);
        builder.setTitle(R.string.addPhoto);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(Registeration.this);
                if (items[item].equals((getString(R.string.take_photo)))) {
                    userChoosenTask = (getString(R.string.take_photo));
                    if (result)
                        cameraIntent();
                } else if (items[item].equals((getString(R.string.choose_from_library)))) {
                    userChoosenTask = (getString(R.string.take_photo));
                    if (result)
                        galleryIntent();
                } else if (items[item].equals((getString(R.string.cancel)))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals((getString(R.string.take_photo))))
                        cameraIntent();
                    else if (userChoosenTask.equals((getString(R.string.choose_from_library))))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
                filePath = data.getData();
                profileIV.setImageBitmap(bitmap);

            } else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
                filePath = data.getData();
                profileIV.setImageBitmap(bitmap);


            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {

        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        bitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
//==================================================

    public void addData(Uri filePath) {
        if (filePath != null) {
            StorageReference childRef = storageRef.child(imgName);
            UploadTask uploadTask = childRef.putFile(filePath);
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                    pd.dismiss();
                    @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                    editor.putString("profileImg", downloadUrl.toString());
                    editor.commit();
                    Toast.makeText(Registeration.this, "Image Uploaded successfully", Toast.LENGTH_SHORT).show();
                    dialog.hide();
                    final Intent mainIntent = new Intent(Registeration.this, NextRegister.class);
                    startActivity(mainIntent);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
//                    pd.dismiss();
                    dialog.hide();
                    Toast.makeText(Registeration.this, " Failed to Upload img ", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(Registeration.this, "Select an image", Toast.LENGTH_SHORT).show();
        }

    }

    //==========================

    public static void setLanguage(Context ctx, LANG selectedLang) {
        if (selectedLang == LANG.ENGLISH) {
            // Change layout to en
            Locale mLocale = new Locale("en");
            Locale.setDefault(mLocale);
            Configuration config = ctx.getResources().getConfiguration();

            if (!config.locale.equals(mLocale)) {
                config.locale = mLocale;
                ctx.getResources().updateConfiguration(config, null);
            }
//            SharedPrefCustom.saveLocalization(ctx, english);
        } else {
            // Change layout to ar
            Locale mLocale = new Locale("ar");
            Locale.setDefault(mLocale);
            Configuration config = ctx.getResources().getConfiguration();
            if (!config.locale.equals(mLocale)) {
                config.locale = mLocale;
                ctx.getResources().updateConfiguration(config, null);
            }
//            SharedPrefCustom.saveLocalization(ctx, arabic);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("language", MODE_PRIVATE);
        String lang = sharedPreferences.getString("lang", "");
        if (lang.equals("ar")) {
            setLanguage(getApplicationContext(), LANG.ARABIC);
        } else {
            setLanguage(getApplicationContext(), LANG.ENGLISH);
        }
    }
}
