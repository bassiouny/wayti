package com.ntamtech.wayti;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.crashlytics.android.Crashlytics;

import config.SaveSharedPreference;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {
    Animation animation;
    Animation animationText;
    ImageView logo;
    ImageView wayti;
    ConstraintLayout constraintLayout;
    Helper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        helper = new Helper();
        helper.hideStatusBar(this);
        setContentView(R.layout.activity_main);
        intial();
        events();
    }

    public void intial(){
        logo = (ImageView)findViewById(R.id.logo);
        wayti = (ImageView)findViewById(R.id.wayti);
        constraintLayout = (ConstraintLayout)findViewById(R.id.container);
        animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.move);
        animationText = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.opacity);
        logo.startAnimation(animation);
        wayti.setVisibility(View.GONE);
        //constraintLayout.getBackground().setAlpha(200);
    }

    public void events(){
        animation.setAnimationListener(new Animation.AnimationListener(){
            @Override
            public void onAnimationStart(Animation arg0) {
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {
            }
            @Override
            public void onAnimationEnd(Animation arg0) {
                wayti.setVisibility(View.VISIBLE);
                wayti.startAnimation(animationText);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if(!SaveSharedPreference.getUserName(MainActivity.this).equals("")) {
                            final Intent mainIntent = new Intent(MainActivity.this, Login.class);
                            startActivity(mainIntent);
                            finish();
                        }else {
                            final Intent mainIntent = new Intent(MainActivity.this, ChooseLanguage.class);
                            startActivity(mainIntent);
                            finish();
                        }
                    }
                }, 1000);
            }
        });
    }
}
