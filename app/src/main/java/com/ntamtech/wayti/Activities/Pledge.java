package com.ntamtech.wayti.Activities;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ntamtech.wayti.Login;
import com.ntamtech.wayti.R;

import config.SaveSharedPreference;

public class Pledge extends AppCompatActivity {

    ImageView backIV;
    Button regBtn;
    private DatabaseReference mDatabase;
    private ProgressDialog dialog;
    TextView pledgeTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pledge);

        intial();
        event();
    }

    public void intial() {
        backIV = (ImageView) findViewById(R.id.back);
        regBtn = (Button) findViewById(R.id.register);
        pledgeTV = (TextView) findViewById(R.id.pledge);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);


    }

    public void event() {
        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater factory = LayoutInflater.from(Pledge.this);
                final View deleteDialogView = factory.inflate(R.layout.custom_pledge_dialog, null);
                final AlertDialog deleteDialog = new AlertDialog.Builder(Pledge.this).create();
                deleteDialog.setView(deleteDialogView);

                final CheckBox checkBox = (CheckBox) deleteDialogView.findViewById(R.id.checkBox);
                final Button submit = (Button) deleteDialogView.findViewById(R.id.submit);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //your business logic
                        if (checkBox.isChecked()) {
                            register();
                        } else {
                            dialog.hide();
                            Toast.makeText(getApplicationContext(), "من فضلك أنقر للموافقة", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                deleteDialog.show();
            }
        });
    }


    public void register() {

        dialog.show();

        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
        final String userId = preferences.getString("id", "");
        final String phone = preferences.getString("phone", "");
        String password = preferences.getString("password", "");
        final String profileImg = preferences.getString("profileImg", "");
        final boolean typeOfWater = preferences.getBoolean("typeOfWater", true);
        final int weight = preferences.getInt("weight", 0);
        final String name = preferences.getString("name", "");
        final String carImg1 = preferences.getString("carImg1", "");
        final String carImg2 = preferences.getString("carImg2", "");
        final String city = preferences.getString("city", "");
        final float price = preferences.getFloat("price",1.0f);

        if (!userId.equals("")) {
            mDatabase.child("driver").child(userId).child("weight").setValue(weight);
            mDatabase.child("driver").child(userId).child("name").setValue(name);
            mDatabase.child("driver").child(userId).child("photo").setValue(profileImg);
            mDatabase.child("driver").child(userId).child("total_rate").setValue(0);
            mDatabase.child("driver").child(userId).child("total_no_of_rate").setValue(0);
            mDatabase.child("driver").child(userId).child("status").setValue(0);
            mDatabase.child("driver").child(userId).child("mobile").setValue(phone);
            mDatabase.child("driver").child(userId).child("car_photos").child("car_img1").setValue(carImg1);
            mDatabase.child("driver").child(userId).child("car_photos").child("car_img2").setValue(carImg2);
            mDatabase.child("driver").child(userId).child("isDrinkable").setValue(typeOfWater);
            mDatabase.child("driver").child(userId).child("city").setValue(city);
            mDatabase.child("driver").child(userId).child("status").setValue(0);
            mDatabase.child("driver").child(userId).child("isActive").setValue(false);
            mDatabase.child("driver").child(userId).child("exit").setValue(false);
            mDatabase.child("driver").child(userId).child("block").setValue(false);
            mDatabase.child("driver").child(userId).child("price").setValue(price);
            mDatabase.child("driver").child(userId).child("password").setValue(password);

            dialog.hide();
            startActivity(new Intent(Pledge.this, Activation.class));
            finish();
        }
    }
}
