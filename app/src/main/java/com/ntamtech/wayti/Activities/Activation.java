package com.ntamtech.wayti.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ntamtech.wayti.R;

import java.util.Random;

import Util.Utility;
import config.RestInterface;
import config.SaveSharedPreference;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Activation extends AppCompatActivity {

    Button submit;
    EditText codeET;
    int code;
    String phone;
    String userId;
    private DatabaseReference mDatabase;
    Button resendCode;
    TextView problemTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activation);

        init();

        getCode();
        events();
    }

    public void getCode() {

        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
        phone = preferences.getString("phone", "");
        userId = preferences.getString("id", "");

        Random rand = new Random();
        code = rand.nextInt(9999);
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.baseURL).build();
        RestInterface restInterface = adapter.create(RestInterface.class);
        restInterface.editPassword("+966" + phone, code, new Callback<String>() {

            @Override
            public void success(String model, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {

                if (error.getKind().equals(RetrofitError.Kind.NETWORK)) {
                    Toast.makeText(getApplicationContext(), "Please Check your Connection!", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    public void init() {

        codeET = (EditText) findViewById(R.id.code);
        submit = (Button) findViewById(R.id.submit);
        resendCode = (Button)findViewById(R.id.resendCode);
        problemTV = (TextView)findViewById(R.id.problem);
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    public void events() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utility.isNotNull(codeET.getText().toString())) {
                    int codeT = Integer.valueOf(codeET.getText().toString());
                    if (codeT == code) {
                        mDatabase.child("driver").child(userId).child("isActive").setValue(true);
                        SaveSharedPreference.setUserName(getApplicationContext(), phone);
                        Intent intent = new Intent(Activation.this, Home.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "Wrong activation code", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), R.string.enter_code, Toast.LENGTH_LONG).show();
                }
            }
        });

        resendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.baseURL).build();
                RestInterface restInterface = adapter.create(RestInterface.class);
                restInterface.editPassword("+966" + phone, code, new Callback<String>() {

                    @Override
                    public void success(String model, Response response) {

                    }

                    @Override
                    public void failure(RetrofitError error) {

                        if (error.getKind().equals(RetrofitError.Kind.NETWORK)) {
                            Toast.makeText(getApplicationContext(), "Please Check your Connection!", Toast.LENGTH_LONG).show();
                        }

                    }
                });

                resendCode.setTextColor(Color.parseColor("#207BD8"));
                resendCode.setEnabled(false);
            }
        });

        problemTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
