package com.ntamtech.wayti.Activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.wayti.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import Bean.User;

public class ForgetPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    ProgressDialog dialog;
    TextView cancel;
    boolean foundUser=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password2);
        cancel = (TextView) findViewById(R.id.cancel);
        findViewById(R.id.login).setOnClickListener(this);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private EditText getPhone() {
        return (EditText) findViewById(R.id.phone);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login:
                if (getPhone().getText().toString().trim().isEmpty() || getPhone().getText().toString().trim().length() < 6) {
                    Toast.makeText(getApplicationContext(), getString(R.string.format_num), Toast.LENGTH_SHORT).show();
                    return;
                }
                dialog.show();
                FirebaseAuth.getInstance().signInWithEmailAndPassword("wayti_admin@gmail.com", "123456").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            foundUser=false;
                            FirebaseDatabase.getInstance().getReference().child("driver").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (DataSnapshot item : dataSnapshot.getChildren()) {
                                        User user = item.getValue(User.class);
                                        if (user.getMobile().equals(getPhone().getText().toString())) {
                                            final String phone = getPhone().getText().toString();
                                            final String password = user.getPassword();
                                            if (password.trim().isEmpty()) {
                                                Toast.makeText(ForgetPasswordActivity.this, "Error Number 124", Toast.LENGTH_SHORT).show();
                                                dialog.dismiss();
                                                return;
                                            }
                                            foundUser=true;
                                            new AsyncTask<String, String, String>() {

                                                @Override
                                                protected String doInBackground(String... params) {
                                                    try {
                                                        String response = makePostRequest("http://ntam.tech/send_sms/send_sms.php",
                                                                "phone=" + phone + "&code=" + password);
                                                        return response;
                                                    } catch (IOException ex) {
                                                        ex.printStackTrace();
                                                        return "Sorry Message Not Send";
                                                    }
                                                }

                                                @Override
                                                protected void onPostExecute(String s) {
                                                    super.onPostExecute(s);
                                                    dialog.dismiss();
                                                    Toast.makeText(ForgetPasswordActivity.this, s, Toast.LENGTH_SHORT).show();
                                                }
                                            }.execute("");
                                            break;
                                        }
                                    }
                                    if(!foundUser){
                                        Toast.makeText(ForgetPasswordActivity.this, "Driver not found", Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();
                                    }
                                }


                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Toast.makeText(ForgetPasswordActivity.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                }
                            });
                        } else {
                            Toast.makeText(ForgetPasswordActivity.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    }
                });
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseAuth.getInstance().signOut();
    }

    public static String makePostRequest(String stringUrl, String payload) throws IOException {
        Log.e("makePostRequest: ", payload);
        URL url = new URL(stringUrl);
        HttpURLConnection uc = (HttpURLConnection) url.openConnection();
        String line;
        StringBuffer jsonString = new StringBuffer();
        uc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        uc.setRequestProperty("charset", "utf-8");
        uc.setRequestMethod("POST");
        uc.setDoInput(true);
        uc.setInstanceFollowRedirects(false);
        uc.connect();
        OutputStreamWriter writer = new OutputStreamWriter(uc.getOutputStream(), "UTF-8");
        writer.write(payload);
        writer.close();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            while ((line = br.readLine()) != null) {
                jsonString.append(line);
            }
            br.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        uc.disconnect();
        JSONObject jObj;
        String resutl;
        try {
            jObj = new JSONObject(jsonString.toString());
            resutl = jObj.getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
            resutl = "Sorry Message Not Send";
        }
        return resutl;
    }
}
