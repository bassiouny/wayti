package com.ntamtech.wayti.Activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.wayti.R;

import java.util.ArrayList;
import java.util.Map;

import Adapter.requestAdapter;;
import Bean.requests;
import Singleton.Singleton;

public class Requests extends AppCompatActivity {

    requestAdapter adapter;
    ListView lv;
    ArrayList<requests> requests = new ArrayList<>();
    private DatabaseReference mDatabase;
    ImageView backIV;
    ImageView backLeftIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requests);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        backIV = (ImageView) findViewById(R.id.back);
        backLeftIV = (ImageView)findViewById(R.id.backLeft);
        lv = (ListView) findViewById(R.id.lv);
        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
        final String userId = preferences.getString("id", "");
        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        SharedPreferences sharedPreferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = sharedPreferences.getString("lang", "");
        if (lang.equals("ar")){
            backLeftIV.setVisibility(View.GONE);
        }else {
            backIV.setVisibility(View.GONE);
        }

        backLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mDatabase.child("request").child("pending").orderByChild("driverID").equalTo(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                requests.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    @SuppressWarnings("unchecked")
                    Map<String, Object> map = (Map<String, Object>) ds.getValue();
                    String username = map.get("username").toString();
                    String user_photo = map.get("user_photo").toString();
                    String user_phone =  map.get("user_phone").toString();
                    String userID = map.get("userID").toString();
                    double user_latitude =Double.parseDouble(map.get("user_latitude").toString());
                    double user_longitude =Double.parseDouble(map.get("user_longitude").toString());
                    requests.add(new requests(userID, username, user_photo,0, user_phone , user_latitude ,user_longitude));
                }
                adapter = new requestAdapter(Requests.this, requests);
                lv.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                //Singleton.getInstance().setRequests(requests);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
//    }
}
