package com.ntamtech.wayti.Activities;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.wayti.R;

import Adapter.SpinnerAdapter;
import Bean.User;

public class ProfileActivity extends AppCompatActivity {

    Spinner typeOfWaterSP, weightSP, citySP;
    ImageView img;
    String[] cityArr;
    String[] weightArr;
    LinearLayout linear;
    ProgressBar progress;
    EditText etPrice;
    Button save,close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        typeOfWaterSP = (Spinner) findViewById(R.id.typeOfWater);
        weightSP = (Spinner) findViewById(R.id.weight);
        citySP = (Spinner) findViewById(R.id.city);
        img = (ImageView) findViewById(R.id.img);
        progress = (ProgressBar) findViewById(R.id.progress);
        linear = (LinearLayout) findViewById(R.id.linear);
        etPrice = (EditText) findViewById(R.id.et_price);
        save = (Button) findViewById(R.id.btn_ok);
        close = (Button) findViewById(R.id.btn_cancel);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        typeOfWaterSP.setAdapter(new SpinnerAdapter(ProfileActivity.this, R.layout.custom_spinner, getResources().getStringArray(R.array.typeOfWater)));
        weightSP.setAdapter(new SpinnerAdapter(ProfileActivity.this, R.layout.custom_spinner, getResources().getStringArray(R.array.weight)));
        citySP.setAdapter(new SpinnerAdapter(ProfileActivity.this, R.layout.custom_spinner, getResources().getStringArray(R.array.cities)));
        cityArr = getResources().getStringArray(R.array.cities);
        weightArr = getResources().getStringArray(R.array.weight);
        setData();
    }

    private void setData() {

        SharedPreferences sharedPreferences = getSharedPreferences("userData", MODE_PRIVATE);
        final String profileImg = sharedPreferences.getString("profileImg", "");
        final String userId = sharedPreferences.getString("id", "");
        Glide.with(getApplicationContext()).load(profileImg).into(img);
        FirebaseDatabase.getInstance().getReference().child("driver").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user == null) {
                    Toast.makeText(ProfileActivity.this, "Sorry can\'t load data", Toast.LENGTH_SHORT).show();
                    return;
                }

                boolean isDrinkable = user.getIsDrinkable();
                String weight = String.valueOf(user.getWeight());
                String city = user.getCity();
                for (int i = 0; i < cityArr.length; i++) {
                    if (cityArr[i].equals(city)) {
                        citySP.setSelection(i);
                    }
                }
                for (int i = 0; i < weightArr.length; i++) {
                    if (weightArr[i].equals(weight)) {
                        weightSP.setSelection(i);
                    }
                }
                if (isDrinkable) {
                    typeOfWaterSP.setSelection(0); // good water
                } else {
                    typeOfWaterSP.setSelection(1); // good bad
                }
                etPrice.setText(String.valueOf(user.getPrice()));
                progress.setVisibility(View.GONE);
                linear.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etPrice.getText().toString().trim().isEmpty()) {
                    Toast.makeText(ProfileActivity.this, R.string.fillData, Toast.LENGTH_SHORT).show();
                } else if (Float.parseFloat(etPrice.getText().toString()) <= 0) {
                    Toast.makeText(ProfileActivity.this, R.string.numberGreaterThanZero, Toast.LENGTH_SHORT).show();
                } else {
                    // save data
                    SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();

                    FirebaseDatabase.getInstance().getReference().child("driver").child(userId).child("city").setValue(citySP.getSelectedItem().toString());
                    editor.putString("city", citySP.getSelectedItem().toString());
                    if (typeOfWaterSP.getSelectedItemPosition() == 0) {
                        FirebaseDatabase.getInstance().getReference().child("driver").child(userId).child("isDrinkable").setValue(true);
                        editor.putBoolean("typeOfWater", true);
                    } else {
                        FirebaseDatabase.getInstance().getReference().child("driver").child(userId).child("isDrinkable").setValue(false);
                        editor.putBoolean("typeOfWater", false);
                    }
                    int weightInt = getWeightIntFromString(weightSP.getSelectedItemPosition());
                    FirebaseDatabase.getInstance().getReference().child("driver").child(userId).child("weight").setValue(weightInt);
                    editor.putInt("weight", weightInt);

                    FirebaseDatabase.getInstance().getReference().child("driver").child(userId).child("price").setValue(Double.parseDouble(etPrice.getText().toString()));
                    Toast.makeText(ProfileActivity.this, "Saved", Toast.LENGTH_SHORT).show();
                    progress.setVisibility(View.VISIBLE);
                    save.setEnabled(false);
                    finish();

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    private int getWeightIntFromString(int index) {
        int result = 12;
        switch (index) {
            case 0:
                result = 12;
                break;
            case 1:
                result = 18;
                break;
            case 2:
                result = 19;
                break;
            case 3:
                result = 28;
                break;
            case 4:
                result = 30;
                break;
            case 5:
                result = 32;
                break;
        }
        return result;
    }
}
