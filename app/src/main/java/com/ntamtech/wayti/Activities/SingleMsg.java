package com.ntamtech.wayti.Activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dewarder.holdinglibrary.HoldingButtonLayout;
import com.dewarder.holdinglibrary.HoldingButtonLayoutListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.ntamtech.wayti.R;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import Bean.Messages;
import Bean.User;
import Util.DownloadListener;
import Util.DownloadUrl;
import Util.MediaUtils;

public class SingleMsg extends AppCompatActivity implements HoldingButtonLayoutListener {

    RelativeLayout relativeLayout;
    private DatabaseReference mDatabase;
    Button sendBtn;
    EditText msgET;
    String key;
    ImageView backIV;
    ScrollView scrollView;
    ImageView backLeftIV;
    TextView nameTV;


    ImageView deleteIV;
    ImageView deleteLeftIV;

    private static final int WRITEEXTERNALSTORAGE = 120;
    private static final DateFormat mFormatter = new SimpleDateFormat("mm:ss:SS");
    private static final float SLIDE_TO_CANCEL_ALPHA_MULTIPLIER = 2.5f;
    private static final long TIME_INVALIDATION_FREQUENCY = 50L;

    private HoldingButtonLayout mHoldingButtonLayout;
    private TextView mTime;
    private View mSlideToCancel;

    private int mAnimationDuration;
    private ViewPropertyAnimator mTimeAnimator;
    private ViewPropertyAnimator mSlideToCancelAnimator;
    private ViewPropertyAnimator mInputAnimator;
    private long mStartTime;
    private Runnable mTimerRunnable;

    MediaRecorder mediaRecorder;
    String AudioSavePathInDevice = null;

    String folderPath = Environment.getExternalStorageDirectory().getPath() + File.separator + R.string.app_name;
    DownloadUrl downloadUrl;
    ProgressBar progress;
    long timeDifference=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_msg);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        init();
        events();
        downloadUrl = DownloadUrl.getInstance(getApplicationContext());
        askPermission();

    }

    public void init(){

        relativeLayout = (RelativeLayout)findViewById(R.id.relativeLayout);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        sendBtn = (Button)findViewById(R.id.send);
        msgET = (EditText)findViewById(R.id.msg);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        nameTV = (TextView)findViewById(R.id.userName);
        backIV = (ImageView) findViewById(R.id.back);
        backLeftIV = (ImageView)findViewById(R.id.backLeft);
        deleteIV = (ImageView) findViewById(R.id.delete);
        deleteLeftIV = (ImageView)findViewById(R.id.deleteLeft);
        progress = (ProgressBar) findViewById(R.id.progress);

        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
        final String driverId = preferences.getString("id", "");
        final String nameDriver = preferences.getString("name", "");
        final String profileImg = preferences.getString("profileImg", "");

        SharedPreferences sp = getSharedPreferences("chatData", MODE_PRIVATE);
        final String userId = sp.getString("userId", "");
        final String name = sp.getString("name", "");
        final String img = sp.getString("img", "");

        SharedPreferences sharedPreferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = sharedPreferences.getString("lang", "");
        if (lang.equals("ar")){
            backLeftIV.setVisibility(View.GONE);
            deleteIV.setVisibility(View.GONE);
        }else {
            backIV.setVisibility(View.GONE);
            deleteLeftIV.setVisibility(View.GONE);
        }

        nameTV.setText(name);
        key = driverId+"-"+userId;
        mDatabase.child("chat").child(key).child("messages").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
                linearLayout.removeAllViews();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    if (ds.getValue(Messages.class).getIsSender().equals("1")){
                        LayoutInflater factory = LayoutInflater.from(SingleMsg.this);
                        final View myViewSender = factory.inflate(R.layout.sender_row, null);
                        TextView msgSenderTV = (TextView) myViewSender.findViewById(R.id.msg);
                        final ImageView senderIV = (ImageView) myViewSender.findViewById(R.id.senderImg);
                        LinearLayout containRecord = (LinearLayout) myViewSender.findViewById(R.id.contain_record);

                        /*msgSenderTV.setText(ds.getValue(Messages.class).getMessage());
                        Glide.with(getApplicationContext()).load(profileImg).into(senderIV);
                        linearLayout.addView(myViewSender);*/
                        if (ds.getValue(Messages.class).getMessage().contains(".m4a")) {
                            final ImageView ivPlay = (ImageView) myViewSender.findViewById(R.id.iv_play);
                            final SeekBar seekBar = (SeekBar) myViewSender.findViewById(R.id.seekbar);
                            containRecord.setVisibility(View.VISIBLE);
                            msgSenderTV.setVisibility(View.GONE);

                            File folder = new File(folderPath);
                            if (!folder.exists()) {
                                folder.mkdir();
                            }
                            String audioName = ds.getValue(Messages.class).getMessage();
                            audioName = audioName.substring(audioName.indexOf("/") + 1, audioName.indexOf("?"));
                            audioName = audioName.substring(audioName.lastIndexOf('/') + 1);
                            final String filePathDevice = folderPath + File.separator + audioName;
                            if(new File(filePathDevice).exists()){
                                playRecord(seekBar, ivPlay,filePathDevice);
                                Glide.with(getApplicationContext()).load(profileImg).into(senderIV);
                                linearLayout.addView(myViewSender);
                            }else {
                                progress.setVisibility(View.VISIBLE);
                                downloadUrl.downloadUrlWithProgress(ds.getValue(Messages.class).getMessage(), filePathDevice, new DownloadListener() {
                                    @Override
                                    public void onDownloadFinish() {
                                        progress.setVisibility(View.GONE);
                                        playRecord(seekBar, ivPlay,filePathDevice);
                                        Glide.with(getApplicationContext()).load(profileImg).into(senderIV);
                                        linearLayout.addView(myViewSender);
                                    }
                                });
                            }


                        } else {
                            containRecord.setVisibility(View.GONE);
                            msgSenderTV.setVisibility(View.VISIBLE);
                            msgSenderTV.setText(ds.getValue(Messages.class).getMessage());
                            Glide.with(getApplicationContext()).load(profileImg).into(senderIV);
                            linearLayout.addView(myViewSender);
                        }
                    }else {
                        /*LayoutInflater factory = LayoutInflater.from(SingleMsg.this);
                        final View myViewReciever = factory.inflate(R.layout.reciever_row, null);
                        TextView msgRecieverTV = (TextView) myViewReciever.findViewById(R.id.msg);
                        ImageView recieverIV = (ImageView) myViewReciever.findViewById(R.id.recieverImg);
                        msgRecieverTV.setText(ds.getValue(Messages.class).getMessage());
                        Glide.with(getApplicationContext()).load(img).into(recieverIV);
                        linearLayout.addView(myViewReciever);*/
                        LayoutInflater factory = LayoutInflater.from(SingleMsg.this);
                        final View myViewReciever = factory.inflate(R.layout.reciever_row, null);
                        TextView msgRecieverTV = (TextView) myViewReciever.findViewById(R.id.msg);
                        final ImageView recieverIV = (ImageView) myViewReciever.findViewById(R.id.recieverImg);
                        LinearLayout containRecord = (LinearLayout) myViewReciever.findViewById(R.id.contain_record);

                        if (ds.getValue(Messages.class).getMessage().contains(".m4a")) {
                            final ImageView ivPlay = (ImageView) myViewReciever.findViewById(R.id.iv_play);
                            final SeekBar seekBar = (SeekBar) myViewReciever.findViewById(R.id.seekbar);
                            containRecord.setVisibility(View.VISIBLE);
                            msgRecieverTV.setVisibility(View.GONE);

                            File folder = new File(folderPath);

                            if (!folder.exists()) {
                                folder.mkdir();
                            }
                            String audioName = ds.getValue(Messages.class).getMessage();
                            audioName = audioName.substring(audioName.indexOf("/") + 1, audioName.indexOf("?"));
                            audioName = audioName.substring(audioName.lastIndexOf('/') + 1);
                            final String filePathDevice = folderPath + File.separator + audioName;
                            if(new File(filePathDevice).exists()){
                                playRecord(seekBar, ivPlay,filePathDevice);
                                Glide.with(getApplicationContext()).load(img).into(recieverIV);
                                linearLayout.addView(myViewReciever);
                            }else {
                                progress.setVisibility(View.VISIBLE);
                                downloadUrl.downloadUrlWithProgress(ds.getValue(Messages.class).getMessage(), filePathDevice, new DownloadListener() {
                                    @Override
                                    public void onDownloadFinish() {
                                        progress.setVisibility(View.GONE);
                                        playRecord(seekBar, ivPlay,filePathDevice);
                                        Glide.with(getApplicationContext()).load(img).into(recieverIV);
                                        linearLayout.addView(myViewReciever);
                                    }
                                });
                            }



                        } else {
                            containRecord.setVisibility(View.GONE);
                            msgRecieverTV.setVisibility(View.VISIBLE);
                            msgRecieverTV.setText(ds.getValue(Messages.class).getMessage());
                            Glide.with(getApplicationContext()).load(img).into(recieverIV);
                            linearLayout.addView(myViewReciever);
                        }
                    }
                }
                scrollView.post(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase.child("chat").child(key).child("driverSeen").setValue("1");
        mHoldingButtonLayout = (HoldingButtonLayout) findViewById(R.id.input_holder);
        mHoldingButtonLayout.addListener(this);

        mTime = (TextView) findViewById(R.id.time);
        mSlideToCancel = findViewById(R.id.slide_to_cancel);


        mAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);

    }

    public void events(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final String currentDateandTime = sdf.format(new Date());

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String msgTxt = msgET.getText().toString().trim();
                if (!msgTxt.isEmpty()){

                    SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
                    final String profileImg = preferences.getString("profileImg", "");

                    Map<String, String> data = new HashMap<String, String>();
                    data.put("isSender", "1");
                    data.put("message", msgTxt);
                    data.put("time_date", currentDateandTime);
                    mDatabase.child("chat").child(key).child("messages").push().setValue(data);
                    mDatabase.child("chat").child(key).child("msg").setValue(msgTxt);
                    mDatabase.child("chat").child(key).child("userSeen").setValue("0");
                    mDatabase.child("chat").child(key).child("userDelete").setValue(false);
                    mDatabase.child("chat").child(key).child("sender_photo").setValue(profileImg);
                    msgET.setText("");
                }
            }
        });

        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        backLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        deleteLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SingleMsg.this);
                alertDialogBuilder
                        .setMessage(getResources().getString(R.string.delete))
                        .setPositiveButton(getResources().getString(R.string.okDelete), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //auth.signOut();
                                mDatabase.getRoot().child("chat").child(key).child("driverDelete").setValue(true);
                                finish();


                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        deleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SingleMsg.this);
                alertDialogBuilder
                        .setMessage(getResources().getString(R.string.delete))
                        .setPositiveButton(getResources().getString(R.string.okDelete), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //auth.signOut();
                                mDatabase.getRoot().child("chat").child(key).child("driverDelete").setValue(true);
                                finish();


                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            }
        });
    }



    @Override
    public void onBeforeExpand() {
        cancelAllAnimations();
        mSlideToCancel.setTranslationX(0f);
        mSlideToCancel.setAlpha(0f);
        mSlideToCancel.setVisibility(View.VISIBLE);
        mSlideToCancelAnimator = mSlideToCancel.animate().alpha(1f).setDuration(mAnimationDuration);
        mSlideToCancelAnimator.start();

        mInputAnimator = msgET.animate().alpha(0f).setDuration(mAnimationDuration);
        mInputAnimator.setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                msgET.setVisibility(View.INVISIBLE);
                mInputAnimator.setListener(null);
            }
        });
        mInputAnimator.start();

        mTime.setTranslationY(mTime.getHeight());
        mTime.setAlpha(0f);
        mTime.setVisibility(View.VISIBLE);
        mTimeAnimator = mTime.animate().translationY(0f).alpha(1f).setDuration(mAnimationDuration);
        mTimeAnimator.start();

    }

    @Override
    public void onExpand() {
        mStartTime = System.currentTimeMillis();
        invalidateTimer();
        if (Build.VERSION.SDK_INT < 23 || (ContextCompat.checkSelfPermission(SingleMsg.this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(SingleMsg.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
            // start record
            startRecord();
        } else {
            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
            mHoldingButtonLayout.cancel();
        }
    }

    @Override
    public void onBeforeCollapse() {
        cancelAllAnimations();
        mSlideToCancelAnimator = mSlideToCancel.animate().alpha(0f).setDuration(mAnimationDuration);
        mSlideToCancelAnimator.setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mSlideToCancel.setVisibility(View.INVISIBLE);
                mSlideToCancelAnimator.setListener(null);
            }
        });
        mSlideToCancelAnimator.start();

        msgET.setAlpha(0f);
        msgET.setVisibility(View.VISIBLE);
        mInputAnimator = msgET.animate().alpha(1f).setDuration(mAnimationDuration);
        mInputAnimator.start();

        mTimeAnimator = mTime.animate().translationY(mTime.getHeight()).alpha(0f).setDuration(mAnimationDuration);
        mTimeAnimator.setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mTime.setVisibility(View.INVISIBLE);
                mTimeAnimator.setListener(null);
            }
        });
        mTimeAnimator.start();
    }

    @Override
    public void onCollapse(boolean isCancel) {
        stopTimer();
        if (isCancel || timeDifference < 2000) {
            Toast.makeText(this, "Record Cancel ", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Uploading", Toast.LENGTH_SHORT).show();
            stopRecord();
        }

    }

    @Override
    public void onOffsetChanged(float offset, boolean isCancel) {
        mSlideToCancel.setTranslationX(-mHoldingButtonLayout.getWidth() * offset);
        mSlideToCancel.setAlpha(1 - SLIDE_TO_CANCEL_ALPHA_MULTIPLIER * offset);
    }

    private void invalidateTimer() {
        mTimerRunnable = new Runnable() {
            @Override
            public void run() {
                mTime.setText(getFormattedTime());
                invalidateTimer();
            }
        };

        mTime.postDelayed(mTimerRunnable, TIME_INVALIDATION_FREQUENCY);
    }

    private void stopTimer() {
        if (mTimerRunnable != null) {
            mTime.getHandler().removeCallbacks(mTimerRunnable);
        }
    }

    private void cancelAllAnimations() {
        if (mInputAnimator != null) {
            mInputAnimator.cancel();
        }

        if (mSlideToCancelAnimator != null) {
            mSlideToCancelAnimator.cancel();
        }

        if (mTimeAnimator != null) {
            mTimeAnimator.cancel();
        }
    }

    private String getFormattedTime() {
        timeDifference = System.currentTimeMillis() - mStartTime;
        return mFormatter.format(new Date(timeDifference));
    }

    private void startRecord() {

        AudioSavePathInDevice =
                Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "AudioRecording.m4a";
        MediaRecorderReady();
        try {
            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void MediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }

    private void stopRecord() {
        try {
            mediaRecorder.stop();
        } catch (RuntimeException stopException) {
            //handle cleanup here
        }
        if (mediaRecorder != null) {
            mediaRecorder.release();
            mediaRecorder = null;
        }
        if (timeDifference > 2000) {
            sendRecordToFirebase();
        }
    }

    private void sendRecordToFirebase() {
        if (AudioSavePathInDevice == null || AudioSavePathInDevice.isEmpty())
            return;
        Uri file = Uri.fromFile(new File(AudioSavePathInDevice));
        StorageReference riversRef = FirebaseStorage.getInstance().getReference().child("audios/" + generateRandomString() + ".m4a");
        progress.setVisibility(View.VISIBLE);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final String currentDateandTime = sdf.format(new Date());
        riversRef.putFile(file)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        @SuppressWarnings("VisibleForTests")
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
                        final String profileImg = preferences.getString("profileImg", "");

                        Map<String, String> data = new HashMap<String, String>();
                        data.put("isSender", "1");
                        data.put("message", downloadUrl.toString());
                        data.put("time_date", currentDateandTime);
                        mDatabase.child("chat").child(key).child("messages").push().setValue(data);
                        mDatabase.child("chat").child(key).child("msg").setValue("Audio");
                        mDatabase.child("chat").child(key).child("userSeen").setValue("0");
                        mDatabase.child("chat").child(key).child("userDelete").setValue(false);
                        mDatabase.child("chat").child(key).child("sender_photo").setValue(profileImg);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                    }
                });
    }

    protected String generateRandomString() {
        /*String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;*/
        Long tsLong = System.currentTimeMillis() / 1000;
        return tsLong.toString();

    }


    private void playRecord(final SeekBar seekBar, final ImageView button_play, String filePathDevice) {
        final MediaPlayer mediaPlayer = new MediaPlayer();
        final MediaUtils utils = new MediaUtils();
        final Handler mHandler = new Handler();
        runRecord(seekBar,mediaPlayer,utils,mHandler,button_play,filePathDevice);
    }
    private void runRecord(final SeekBar seekBar , final MediaPlayer mediaPlayer, final MediaUtils utils, final Handler mHandler, final ImageView button_play,String filePathDevice){
        final Runnable mUpdateTimeTask = new Runnable() {
            public void run() {
                long totalDuration = mediaPlayer.getDuration();
                long currentDuration = mediaPlayer.getCurrentPosition();
                // Displaying Total Duration time
                // Displaying time completed playing
                // Updating progress bar
                int progress = (utils.getProgressPercentage(currentDuration, totalDuration));
                seekBar.setProgress(progress);

                // Running this thread after 100 milliseconds
                mHandler.postDelayed(this, 30);
            }
        };
        // Listeners

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                long currentDuration = mediaPlayer.getCurrentPosition();
                // TODO : SET TIME
                //audioViewHolder.tv_music_duration.setText("" + audioViewHolder.utils.milliSecondsToTimer(currentDuration));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mHandler.removeCallbacks(mUpdateTimeTask);


            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mHandler.removeCallbacks(mUpdateTimeTask);
                int totalDuration = mediaPlayer.getDuration();
                int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);
                //audioViewHolder.tv_music_duration.setText("" + audioViewHolder.utils.milliSecondsToTimer(totalDuration));
                // forward or backward to certain seconds
                mediaPlayer.seekTo(currentPosition);

                // update timer progress again
                mHandler.postDelayed(mUpdateTimeTask, 100);

            }
        }); // Important
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                button_play.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                seekBar.setProgress(0);
            }
        }); // Important


        button_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mediaPlayer.isPlaying()) {
                    if (mediaPlayer != null) {
                        mediaPlayer.pause();
                        // Changing button image to play button
                        button_play.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                    }
                } else {
                    // Resume song
                    if (mediaPlayer != null) {
                        mediaPlayer.start();
                        // Changing button image to pause button
                        button_play.setImageResource(R.drawable.ic_pause_black_24dp);
                    }
                }
            }
        });

        try {
            mediaPlayer.reset();
            mediaPlayer.setDataSource(filePathDevice);
            mediaPlayer.prepare();
            seekBar.setProgress(0);
            seekBar.setMax(100);

            // Updating progress bar
            mHandler.postDelayed(mUpdateTimeTask, 100);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void askPermission(){
        boolean hasPermission = (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(SingleMsg.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITEEXTERNALSTORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode ==WRITEEXTERNALSTORAGE && (grantResults.length > 0) &&
                grantResults[0] == PackageManager.PERMISSION_DENIED){
            askPermission();
        }
    }
}
