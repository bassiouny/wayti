package com.ntamtech.wayti.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.ntamtech.wayti.R;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import Adapter.SpinnerAdapter;
import Util.Utility;

public class NextRegister extends AppCompatActivity {

    ImageView backIV;
    Spinner typeOfWaterSP;
    Spinner weightSP;
    ImageView carImg1;
    ImageView carImg2;
    Button selectImgBtn;
    ImageView deleteImg1;
    ImageView deleteImg2;
    Button nextBtn;
    String userChoosenTask;
    EditText price;
    private int REQUEST_CAMERA = 0;
    private int SELECT_FILE = 1;
    Uri filePathCarImg1;
    Uri filePathCarImg2;
    int flag;

    private ProgressDialog dialog;

    Bitmap bitmap;

    StorageReference storageRef;
    String imgName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_register);
        intail();
        events();
    }
    public void intail() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        backIV = (ImageView) findViewById(R.id.back);
        typeOfWaterSP = (Spinner)findViewById(R.id.typeOfWater);
        weightSP = (Spinner)findViewById(R.id.weight);
        carImg1 = (ImageView)findViewById(R.id.carImg1);
        carImg2 = (ImageView)findViewById(R.id.carImg2);
        selectImgBtn = (Button)findViewById(R.id.selectImg);
        deleteImg1=(ImageView)findViewById(R.id.delete_img1);
        deleteImg2=(ImageView)findViewById(R.id.delete_img2);
        nextBtn = (Button)findViewById(R.id.next);
        price = (EditText) findViewById(R.id.price);
        deleteImg1.setVisibility(View.GONE);
        deleteImg2.setVisibility(View.GONE);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Uploading...");
        dialog.setCancelable(false);

        storageRef = FirebaseStorage.getInstance().getReference().child("/images");
        imgName = UUID.randomUUID().toString();
        typeOfWaterSP.setAdapter(new SpinnerAdapter(NextRegister.this, R.layout.custom_spinner, getResources().getStringArray(R.array.typeOfWater)));
        weightSP.setAdapter(new SpinnerAdapter(NextRegister.this, R.layout.custom_spinner, getResources().getStringArray(R.array.weight)));

    }

    public void events() {
        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        selectImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        deleteImg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carImg1.setImageDrawable(null);
                deleteImg1.setVisibility(View.GONE);
                SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                editor.putString("carImg1", "");
                editor.commit();
            }
        });
        deleteImg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carImg2.setImageDrawable(null);
                deleteImg2.setVisibility(View.GONE);
                SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                editor.putString("carImg2", "");
                editor.commit();
            }
        });
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String weight = weightSP.getSelectedItem().toString();
                String typeOfWater = typeOfWaterSP.getSelectedItem().toString();
                Boolean typeOfWaterB;
                String[] typeOfWaterArr = getResources().getStringArray(R.array.typeOfWater);
                if (typeOfWater.equals(typeOfWaterArr[0])){
                    typeOfWaterB = true;
                }else {
                    typeOfWaterB = false;
                }

                if (Utility.isNotNull(typeOfWater)&& Utility.isNotNull(weight)&&Utility.isNotNull(price.getText().toString())){
                    if (filePathCarImg1 !=null && Utility.isNotNull(filePathCarImg1.toString()) && Integer.parseInt(price.getText().toString())>0){

                        String[] parts = weight.split(" ");
                        String part1 = parts[0];

                        SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                        editor.putInt("weight",Integer.valueOf(part1));
                        editor.putBoolean("typeOfWater", typeOfWaterB);
                        editor.putFloat("price",Float.valueOf(price.getText().toString()));
                        editor.commit();
                        final Intent mainIntent = new Intent(NextRegister.this, Pledge.class);
                        startActivity(mainIntent);
                    }else {
                        Toast.makeText(getApplicationContext(),"يجب أدراك صوره للسيارة", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"يرجى ملئ الحقول", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(NextRegister.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(NextRegister.this);
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            if (flag == 0){
                filePathCarImg1 = data.getData();
                addData(0,filePathCarImg1);
            }
            if (flag == 1){
                filePathCarImg2 = data.getData();
                addData(1,filePathCarImg2);
            }
            else if (requestCode == REQUEST_CAMERA){
                onCaptureImageResult(data);
                if (flag == 0){
                    filePathCarImg1 = data.getData();
                    addData(0,filePathCarImg1);
                }
                if (flag == 1){
                    filePathCarImg2 = data.getData();
                    addData(1,filePathCarImg2);
                }
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {

        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


       if (carImg1.getDrawable() == null){
           flag = 0;
           deleteImg1.setVisibility(View.VISIBLE);
       }else {
           flag = 1;
           deleteImg2.setVisibility(View.VISIBLE);
       }

    }

    private void onCaptureImageResult(Intent data) {
        bitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (carImg1.getDrawable() == null){

            deleteImg1.setVisibility(View.VISIBLE);
        }else {
            deleteImg2.setVisibility(View.VISIBLE);
        }

    }

    //==========================================================


    public void addData(final int imgNum, Uri filePath){
        if(filePath != null){
            dialog.show();
            StorageReference childRef;
            if (flag ==0){
                childRef = storageRef.child(imgName+"0");
            }else {
                childRef = storageRef.child(imgName+"1");
            }
            UploadTask uploadTask = childRef.putFile(filePath);
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                    pd.dismiss();
                    @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    if (imgNum == 0){
                        SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                        editor.putString("carImg1", downloadUrl.toString());
                        editor.commit();
                        dialog.hide();
                        Toast.makeText(NextRegister.this, "Image Uploaded successfully", Toast.LENGTH_SHORT).show();
                        carImg1.setImageBitmap(bitmap);

                    }else {
                        SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                        editor.putString("carImg2", downloadUrl.toString());
                        editor.commit();
                        dialog.hide();
                        Toast.makeText(NextRegister.this, "Image Uploaded successfully", Toast.LENGTH_SHORT).show();
                        carImg2.setImageBitmap(bitmap);
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
//                    pd.dismiss();
                    dialog.hide();
                    Toast.makeText(NextRegister.this, " Failed to Upload img ", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else {
            Toast.makeText(NextRegister.this, "Select an image", Toast.LENGTH_SHORT).show();
        }

    }
}
