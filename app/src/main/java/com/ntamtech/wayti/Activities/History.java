package com.ntamtech.wayti.Activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.wayti.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Adapter.historyAdapter;
import Bean.historyBean;
import Bean.requests;

public class History extends AppCompatActivity {

    historyAdapter adapter;
    ListView lv;
    ImageView backLeftIV;
    ArrayList<historyBean> historyBeens=new ArrayList<>();
    private DatabaseReference mDatabase;
    ImageView backIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        lv= (ListView)findViewById(R.id.lv);
        backLeftIV = (ImageView)findViewById(R.id.backLeft);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        backIV = (ImageView)findViewById(R.id.back);

        SharedPreferences sharedPreferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = sharedPreferences.getString("lang", "");
        if (lang.equals("ar")){
            backLeftIV.setVisibility(View.GONE);
        }else {
            backIV.setVisibility(View.GONE);
        }

        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
        final String userId = preferences.getString("id", "");

        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        backLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mDatabase.child("request").child("accepted").orderByChild("driverID").equalTo(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                historyBeens.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren())
                {
                    @SuppressWarnings("unchecked")
                    Map<String, Object> map = (Map<String, Object>) ds.getValue();
                    String username = map.get("username").toString();
                    String date =map.get("datetime").toString();
                    String user_phone =map.get("user_phone").toString();
                    String user_photo =map.get("user_photo").toString();
                    double user_latitude =Double.parseDouble(map.get("user_latitude").toString());
                    double user_longitude =Double.parseDouble(map.get("user_longitude").toString());
                    String status =map.get("status").toString();
                    if (status.equals("1")) {
                        historyBeens.add(new historyBean(username, user_phone, date, user_photo , user_latitude ,user_longitude));
                    }
                }
                adapter = new historyAdapter(History.this,historyBeens);
                lv.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

}
