package com.ntamtech.wayti.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.wayti.Login;
import com.ntamtech.wayti.R;
import com.ntamtech.wayti.Registeration;

import java.util.ArrayList;
import java.util.Map;

import Adapter.chatAdapter;
import Adapter.historyAdapter;
import Bean.chatBean;
import Bean.historyBean;

public class Chat extends AppCompatActivity {

    chatAdapter adapter;
    ListView lv;
    ImageView backLeftIV;
    ArrayList<chatBean> chatBeen = new ArrayList<>();
    private DatabaseReference mDatabase;
    ImageView backIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        init();
        events();

        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
        final String userId = preferences.getString("id", "");

        mDatabase.child("chat").orderByChild("sender_id").equalTo(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                chatBeen.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    @SuppressWarnings("unchecked")
                    Map<String, Object> map = (Map<String, Object>) ds.getValue();
                    String receiver_photo = map.get("receiver_photo").toString();
                    String receiver_name = map.get("receiver_name").toString();
                    String receiver_id = map.get("receiver_id").toString();
                    String msg = map.get("msg").toString();
                    String seen = map.get("driverSeen").toString();
                    Boolean driverDelete = (Boolean) map.get("driverDelete");
                    if (driverDelete == false) {
                        chatBeen.add(new chatBean(receiver_id, receiver_photo, receiver_name, msg, seen));
                    }

                }
                adapter = new chatAdapter(Chat.this, chatBeen);
                lv.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void init(){

        backLeftIV = (ImageView)findViewById(R.id.backLeft);
        lv = (ListView) findViewById(R.id.lv);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        backIV = (ImageView) findViewById(R.id.back);
        SharedPreferences sharedPreferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = sharedPreferences.getString("lang", "");
        if (lang.equals("ar")){
            backLeftIV.setVisibility(View.GONE);
        }else {
            backIV.setVisibility(View.GONE);
        }

    }

    public  void events(){

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Intent mainIntent = new Intent(Chat.this,SingleMsg.class);
                startActivity(mainIntent);
                SharedPreferences.Editor editor = getSharedPreferences("chatData", MODE_PRIVATE).edit();
                editor.putString("userId", chatBeen.get(position).getReceiverID());
                editor.putString("name", chatBeen.get(position).getName());
                editor.putString("img", chatBeen.get(position).getImg());
                editor.commit();

            }
        });

        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        backLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
