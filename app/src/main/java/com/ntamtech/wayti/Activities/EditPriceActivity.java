package com.ntamtech.wayti.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ntamtech.wayti.R;

public class EditPriceActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private ProgressDialog dialog;
    Button btnOK;
    EditText etPrice;
    SharedPreferences preferences;
    String userId;
    Float price;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_price);
        btnOK = (Button) findViewById(R.id.btn_ok);
        etPrice = (EditText) findViewById(R.id.et_price);
        this.setFinishOnTouchOutside(false);
        setTitle("Change Price");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        loadData();
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etPrice.getText().toString().trim().isEmpty()){
                    Toast.makeText(EditPriceActivity.this, R.string.fillData, Toast.LENGTH_SHORT).show();
                }else if (Float.parseFloat(etPrice.getText().toString())<=0){
                    Toast.makeText(EditPriceActivity.this, R.string.numberGreaterThanZero, Toast.LENGTH_SHORT).show();
                }else {
                    UpdatePrice();
                }
            }
        });
    }

    public void UpdatePrice() {

        dialog.show();
        if (!userId.equals("")) {
            mDatabase.child("driver").child(userId).child("price").setValue(Float.parseFloat(etPrice.getText().toString()));
            SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
            editor.putFloat("price",Float.parseFloat(etPrice.getText().toString()));
            editor.commit();
            dialog.hide();
            finish();
        }
    }
    private void loadData(){

        preferences = getSharedPreferences("userData", MODE_PRIVATE);
        userId = preferences.getString("id", "");
        price = preferences.getFloat("price",1f);
        etPrice.setText(price+"");
    }
}
