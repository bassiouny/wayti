package com.ntamtech.wayti.Activities;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.makeramen.roundedimageview.RoundedImageView;
import com.ntamtech.wayti.R;

public class ContactUs extends FragmentActivity implements OnMapReadyCallback {

    ImageView backIV;
    ImageView backLeftIV;
    GoogleMap map;
    TextView emailTTV;
    TextView addressTTV;
    TextView phoneTTV;
    private DatabaseReference mDatabase;
    TextView emailTV;
    TextView phoneTV;
    TextView addressTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        intial();
        events();
    }

    public void intial(){
        backIV = (ImageView)findViewById(R.id.back);
        emailTTV = (TextView)findViewById(R.id.emailTxt);
        addressTTV =  (TextView)findViewById(R.id.addressTxt);
        phoneTTV = (TextView)findViewById(R.id.phoneTxt);
        emailTV = (TextView)findViewById(R.id.email);
        phoneTV = (TextView)findViewById(R.id.phone);
        addressTV = (TextView)findViewById(R.id.address);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        backLeftIV = (ImageView)findViewById(R.id.backLeft);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map2);
        mapFragment.getMapAsync(this);

        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "");
        Drawable imgPhone = getResources().getDrawable( R.mipmap.phone );
        Drawable imgAddress = getResources().getDrawable( R.mipmap.address );
        Drawable imgEmail = getResources().getDrawable( R.mipmap.email );

        if (lang.equals("ar")){
            phoneTTV.setCompoundDrawablesWithIntrinsicBounds(null ,null , imgPhone, null);
            addressTTV.setCompoundDrawablesWithIntrinsicBounds( null,null , imgAddress, null);
            emailTTV.setCompoundDrawablesWithIntrinsicBounds( null,null , imgEmail, null);
            backLeftIV.setVisibility(View.GONE);
        }else{
            phoneTTV.setCompoundDrawablesWithIntrinsicBounds( imgPhone, null, null, null);
            addressTTV.setCompoundDrawablesWithIntrinsicBounds( imgAddress, null, null, null);
            emailTTV.setCompoundDrawablesWithIntrinsicBounds( imgEmail, null, null, null);
            backIV.setVisibility(View.GONE);
        }
    }

    public void events(){
        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        backLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mDatabase.child("setting").child("contact").child("address").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                addressTV.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mDatabase.child("setting").child("contact").child("email").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                emailTV.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mDatabase.child("setting").child("contact").child("phone").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                phoneTV.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        LatLng pp = new LatLng(24.425557,39.494964);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(pp);
        map.addMarker(markerOptions);
        map.moveCamera(CameraUpdateFactory.newLatLng(pp));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
