package com.ntamtech.wayti.Activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.wayti.R;

import java.util.ArrayList;
import java.util.Map;

import Adapter.PaymentHistoryAdapter;
import Adapter.historyAdapter;
import Bean.PaymentHistoryBean;
import Bean.historyBean;
import Singleton.Singleton;

public class CommissionHistory extends AppCompatActivity {

    PaymentHistoryAdapter adapter;
    ListView historyLV;
    ImageView backIV;
    ImageView backLeftIV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commission_history);
        init();



    }

    public void init(){


        historyLV = (ListView)findViewById(R.id.historyLV);
        backIV = (ImageView)findViewById(R.id.back);
        backLeftIV = (ImageView)findViewById(R.id.backLeft);

        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "");
        if (lang.equals("ar")){
            backLeftIV.setVisibility(View.GONE);
        }else {
            backIV.setVisibility(View.GONE);
        }

        ArrayList<PaymentHistoryBean> historyBeens = Singleton.getInstance().getHistoryBeens();
        adapter = new PaymentHistoryAdapter(CommissionHistory.this,historyBeens);
        if (adapter!=null){
            historyLV.setAdapter(adapter);
        }
        adapter.notifyDataSetChanged();

        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        backLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
