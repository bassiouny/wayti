package com.ntamtech.wayti.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.wayti.MainActivity;
import com.ntamtech.wayti.R;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import Bean.PaymentHistoryBean;
import Singleton.Singleton;

public class Commission extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    ImageView backIV;
    TextView noOfAcceptTV;
    TextView noOfRejectTV;
    Button saveCommission;
    ImageView backLeftIV;
    Button history;
    TextView totalPaymentTV;
    String userID;
    ArrayList<PaymentHistoryBean> historyBeens=new ArrayList<>();
    private DatabaseReference mDatabase;

    TextView paidTV;
    TextView restTV;
    double totalPayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commission);

        intial();
        events();

    }

    public void intial() {
        backIV = (ImageView) findViewById(R.id.back);
        noOfAcceptTV = (TextView) findViewById(R.id.noOfAccept);
        noOfRejectTV = (TextView) findViewById(R.id.noOfReject);
        saveCommission = (Button) findViewById(R.id.save);
        totalPaymentTV = (TextView) findViewById(R.id.totalPayment);
        history = (Button) findViewById(R.id.history);
        SharedPreferences sp = getSharedPreferences("userData", MODE_PRIVATE);
        userID = sp.getString("id", "");
        final String noOfAccept = sp.getString("noOfAccept", "");
        final String noOfReject = sp.getString("noOfReject", "");
        final int weight = sp.getInt("weight", 0);
        noOfAcceptTV.setText(noOfAccept);
        noOfRejectTV.setText(noOfReject);

        String[] parts = String.valueOf(weight).split(" ");
        String part1 = parts[0]; // 004
        totalPayment = Integer.valueOf(part1) * Integer.valueOf(noOfAccept) * 0.5;
        totalPaymentTV.setText(totalPayment + "");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        paidTV = (TextView)findViewById(R.id.paid);
        restTV = (TextView)findViewById(R.id.rest);
        backLeftIV = (ImageView)findViewById(R.id.backLeft);
        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "");
        if (lang.equals("ar")){
            backLeftIV.setVisibility(View.GONE);
        }else {
            backIV.setVisibility(View.GONE);
        }

    }

    public void events() {

        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        saveCommission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater factory = LayoutInflater.from(Commission.this);
                final View paymentDialogView = factory.inflate(R.layout.payment_dialog, null);
                final AlertDialog paymentDialog = new AlertDialog.Builder(Commission.this).create();
                paymentDialog.setView(paymentDialogView);
                final EditText priceET = (EditText) paymentDialogView.findViewById(R.id.price);
                final EditText bankNameET = (EditText) paymentDialogView.findViewById(R.id.bankName);
                final EditText nameET = (EditText) paymentDialogView.findViewById(R.id.name);
                final EditText numET = (EditText) paymentDialogView.findViewById(R.id.num);
                final EditText dateET = (EditText) paymentDialogView.findViewById(R.id.date);
                dateET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                        Calendar c = Calendar.getInstance();
                        int mYear = c.get(Calendar.YEAR);
                        int mMonth = c.get(Calendar.MONTH);
                        int mDay = c.get(Calendar.DAY_OF_MONTH);
                        DatePickerDialog datePickerDialog = new DatePickerDialog(
                                Commission.this, Commission.this, mYear, mMonth, mDay);
                        datePickerDialog.show();

                        dateET.setText(mDay+"-"+mMonth+"-"+mYear);
                    }
                });
                paymentDialog.setCancelable(true);
                paymentDialogView.findViewById(R.id.pay).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            double price = Double.parseDouble(priceET.getText().toString());
                            String bankName = bankNameET.getText().toString().trim();
                            String name = nameET.getText().toString().trim();
                            String num = numET.getText().toString().trim();
                            String date = dateET.getText().toString().trim();
                            if (!String.valueOf(price).isEmpty() && !bankName.isEmpty() && !name.isEmpty() && !num.isEmpty() && !date.isEmpty()) {

                                Map<String, Object> complainData = new HashMap<String, Object>();
                                complainData.put("bank_name", bankName);
                                complainData.put("date_time", date);
                                complainData.put("driverID", userID);
                                complainData.put("driver_name", name);
                                complainData.put("payment_no", num);
                                complainData.put("price", price);
                                mDatabase.child("payment").push().setValue(complainData);
                                paymentDialog.dismiss();
                                Toast.makeText(getApplicationContext(),"تم التسجيل بنجاح", Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(getApplicationContext(),"يجب ملئ جميع البيانات", Toast.LENGTH_SHORT).show();
                            }
                        }catch (Exception e){
                            Toast.makeText(Commission.this, "Please try again", Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }
                });

                paymentDialog.show();
            }
        });

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Commission.this, CommissionHistory.class));
            }
        });
        backLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        commissionHistory();
    }

    public void commissionHistory(){

        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
        final String userId = preferences.getString("id", "");

        mDatabase.child("payment").orderByChild("driverID").equalTo(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                historyBeens.clear();
                double paid=0;
                for (DataSnapshot ds : dataSnapshot.getChildren())
                {
                    @SuppressWarnings("unchecked")
                    Map<String, Object> map = (Map<String, Object>) ds.getValue();
                    String bank_name = map.get("bank_name").toString();
                    String date =map.get("date_time").toString();
                    String payment_no =map.get("payment_no").toString();
                    String price =map.get("price").toString();
                    paid+= Double.parseDouble(price);
                    historyBeens.add(new PaymentHistoryBean(bank_name, date, payment_no, price));

                }
                Singleton.getInstance().setHistoryBeens(historyBeens);
                paidTV.setText(paid+"");
                double rest = totalPayment-paid;
                restTV.setText(rest+"");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

    }
}
