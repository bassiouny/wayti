package com.ntamtech.wayti.Activities;


import android.app.Notification;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.wayti.Fragments.MapFragment;
import com.ntamtech.wayti.Login;
import com.ntamtech.wayti.R;
import com.ntamtech.wayti.Registeration;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

import Adapter.CustomDrawerAdapter;
import Adapter.requestAdapter;
import Bean.DrawerItem;
import Bean.requests;
import Singleton.Singleton;
import br.com.goncalves.pugnotification.notification.PugNotification;
import config.LANG;
import config.SaveSharedPreference;

public class Home extends AppCompatActivity {
    DrawerLayout drawer;
    ListView drawerList;
    ListView drawerListLeft;
    ImageView menuIV;
    ImageView notificationIV;
    ImageView menuIVAr;
    ImageView notificationIVAr;
    MapFragment mapFragment;
    FragmentManager manager;
    LinearLayout linearLayout;
    NavigationView navigationViewLeft;
    NavigationView navigationViewRight;
    Button commissionBtn;
    TextView noOfAcceptTV;
    TextView noOfRejectTV;
    DatabaseReference mDatabase;
    TextView notifyLeftTV;
    TextView notifyRightTV;
  //  private NotificationDelegater mDelegater;
   // private NotificationGlobal mGlobal;
    ArrayList<requests> requests = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "");
        if (lang.equals("ar")) {
            SetLanguage(getApplicationContext(), LANG.ARABIC);
        }
        setContentView(R.layout.activity_home);

        init();
        if (lang.equals("ar")) {
            SetLanguage(getApplicationContext(), LANG.ARABIC);
            menuIVAr.setVisibility(View.GONE);
            notificationIVAr.setVisibility(View.GONE);
            notifyRightTV.setVisibility(View.GONE);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.nav_left_view));
            menuIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.openDrawer(Gravity.END);
                }
            });
        } else {
            SetLanguage(getApplicationContext(), LANG.ENGLISH);
            menuIV.setVisibility(View.GONE);
            notificationIV.setVisibility(View.GONE);
            notifyLeftTV.setVisibility(View.GONE);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.nav_right_view));

            menuIVAr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.openDrawer(Gravity.START);
                }
            });

        }

        SharedPreferences sp = getSharedPreferences("userData", MODE_PRIVATE);
        final String userId = sp.getString("id", "");

        mDatabase.child("request").child("accepted").orderByChild("driverID").equalTo(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                int size = 0;
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    if (postSnapshot.getValue(requests.class).getStatus()==1) {
                        size++;
                    }
                }
                noOfAcceptTV = (TextView) findViewById(R.id.noOfAccept);
                noOfAcceptTV.setText(size + "");
                SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                editor.putString("noOfAccept", size + "");
                editor.commit();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mDatabase.child("request").child("failed").orderByChild("driverID").equalTo(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                noOfRejectTV = (TextView) findViewById(R.id.noOfReject);
                noOfRejectTV.setText(dataSnapshot.getChildrenCount() + "");
                SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                editor.putString("noOfReject", dataSnapshot.getChildrenCount() + "");
                editor.commit();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        mDatabase.child("request").child("pending").orderByChild("driverID").equalTo(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                requests.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Map<String, Object> map = (Map<String, Object>) postSnapshot.getValue();
                    String username = map.get("username").toString();
                    String user_photo = map.get("user_photo").toString();
                    String user_phone = map.get("user_phone").toString();
                    String userID = map.get("userID").toString();

                    double user_latitude = Double.parseDouble(map.get("user_latitude").toString());
                    double user_longitude = Double.parseDouble(map.get("user_longitude").toString());

                    requests.add(new requests(userID, username, user_photo, 0, user_phone,user_latitude,user_longitude));
                }
                if (Singleton.getInstance().getRequests() != null) {

                    SharedPreferences sp = getSharedPreferences("notification", MODE_PRIVATE);
                    int noti = sp.getInt("num", 0);
                    if (noti < dataSnapshot.getChildrenCount()) {

//                        NotificationBuilder.V1 builderG = NotificationBuilder.global()
//                                .setIconDrawable(getResources().getDrawable(R.mipmap.ic_launcher))
//                                .setTitle("aaaaaa")
//                                .setText("aaaaaaaa");
//                        mDelegater.send(builderG.getNotification());
//                        Singleton.getInstance().setRequests(requests);

                        PugNotification.with(getApplicationContext())
                                .load()

                                .title("Wayti")
                                .message("لديك طلب توصيل الان")
                                .smallIcon(R.mipmap.ic_launcher)
                                .largeIcon(R.mipmap.ic_launcher)
                                .flags(Notification.DEFAULT_ALL)
                                .color(R.color.white)

                                .simple()
                                .build();
                        SharedPreferences.Editor editor = getSharedPreferences("notification", MODE_PRIVATE).edit();
                        editor.putInt("num", requests.size());
                        editor.commit();

                        requests.removeAll(Singleton.getInstance().getRequests());

                        //  Singleton.getInstance().setRequests(requests);
//                        adapter = new requestAdapter(Home.this, requests);
                    }
                } else {
                    SharedPreferences.Editor editor = getSharedPreferences("notification", MODE_PRIVATE).edit();
                    editor.putInt("num", requests.size());
                    editor.commit();
                    Singleton.getInstance().setRequests(requests);

//                    adapter = new requestAdapter(Home.this, requests);
                }
                notifyLeftTV.setText(dataSnapshot.getChildrenCount() + "");
                notifyRightTV.setText(dataSnapshot.getChildrenCount() + "");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        View headerLeft = navigationViewLeft.getHeaderView(0);
        ImageView profileIV = (ImageView) headerLeft.findViewById(R.id.profile_image);
        TextView nameTV = (TextView) headerLeft.findViewById(R.id.name);
        TextView phoneTV = (TextView) headerLeft.findViewById(R.id.phone);

        View headerRight = navigationViewRight.getHeaderView(0);
        ImageView profileIVR = (ImageView) headerRight.findViewById(R.id.profile_image);
        TextView nameTVR = (TextView) headerRight.findViewById(R.id.name);
        TextView phoneTVR = (TextView) headerRight.findViewById(R.id.phone);



        SharedPreferences sharedPreferences = getSharedPreferences("userData", MODE_PRIVATE);
        final String phone = sharedPreferences.getString("phone","");
        final String profileImg = sharedPreferences.getString("profileImg", "");
        final String name = sharedPreferences.getString("name", "");

        Glide.with(getApplicationContext()).load(profileImg).into(profileIV);
        nameTV.setText(phone+"");
        phoneTV.setText(name);

        Glide.with(getApplicationContext()).load(profileImg).into(profileIVR);
        nameTVR.setText(phone+"");
        phoneTVR.setText(name);

        final ArrayList<DrawerItem> dataList = new ArrayList<DrawerItem>();
        dataList.add(new DrawerItem(getResources().getString(R.string.profile), R.drawable.ic_profile2));
        dataList.add(new DrawerItem(getResources().getString(R.string.map), R.drawable.map));
        dataList.add(new DrawerItem(getResources().getString(R.string.conversations), R.drawable.conversations));
        dataList.add(new DrawerItem(getResources().getString(R.string.CommissionPayment), R.drawable.exchange));
        dataList.add(new DrawerItem(getResources().getString(R.string.history), R.drawable.history));
        dataList.add(new DrawerItem(getResources().getString(R.string.complaints), R.drawable.complaints));
        dataList.add(new DrawerItem(getResources().getString(R.string.aboutUs), R.drawable.about_us));
        dataList.add(new DrawerItem(getResources().getString(R.string.contactUs), R.drawable.contact_us));
        dataList.add(new DrawerItem(getResources().getString(R.string.exit), R.drawable.logout));

        drawerList.setAdapter(new CustomDrawerAdapter(Home.this, R.layout.custom_drawer_item, dataList));
        drawerListLeft.setAdapter(new CustomDrawerAdapter(Home.this, R.layout.custom_drawer_item_left, dataList));

        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                navigation(position);
                drawer.closeDrawer(GravityCompat.END);
            }
        });
        drawerListLeft.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                navigation(position);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        mapFragment = new MapFragment();
        manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.mainLayout, mapFragment).commit();

        commissionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent mainIntent = new Intent(Home.this, Commission.class);
                startActivity(mainIntent);
            }
        });

        notificationIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent mainIntent = new Intent(Home.this, Requests.class);
                startActivity(mainIntent);
            }
        });
        notificationIVAr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent mainIntent = new Intent(Home.this, Requests.class);
                startActivity(mainIntent);
            }
        });

    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {  /*Closes the Appropriate Drawer*/
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
    }

    public void navigation(int position) {
        position = position-1;
        if (position == 0) {
//            MapFragment mapFragment = new MapFragment();
//            FragmentManager manager = getSupportFragmentManager();
//            manager.beginTransaction().replace(R.id.mainLayout, mapFragment).commit();
            drawer.closeDrawer(GravityCompat.START);
        }
        if (position == 1) {

            final Intent mainIntent = new Intent(Home.this, Chat.class);
            startActivity(mainIntent);
        }
        if (position == 5) {

            final Intent mainIntent = new Intent(Home.this, AboutUs.class);
            startActivity(mainIntent);
        }
        if (position == 2) {

            final Intent mainIntent = new Intent(Home.this, Commission.class);
            startActivity(mainIntent);
        }
        if (position == 3) {

            final Intent mainIntent = new Intent(Home.this, History.class);
            startActivity(mainIntent);
        }
        if (position == 6) {

            final Intent mainIntent = new Intent(Home.this, ContactUs.class);
            startActivity(mainIntent);
        }

        if (position == 4) {

            final Intent mainIntent = new Intent(Home.this, complains.class);
            startActivity(mainIntent);
        }

        if (position == 7) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Home.this);
            alertDialogBuilder
                    .setMessage(getResources().getString(R.string.logout))
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //auth.signOut();
                            SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
                            final String driverId = preferences.getString("id", "");
                            mDatabase.child("driver").child(driverId).child("exit").setValue(true);
                            SaveSharedPreference.clearUserName(getApplicationContext());
                            final Intent mainIntent = new Intent(getApplicationContext(), Login.class);
                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(mainIntent);
                            finish();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        if (position == -1 ){
            Intent mainIntent = new Intent(Home.this, ProfileActivity.class);
            startActivity(mainIntent);
        }
    }
//==========================

    public static void SetLanguage(Context ctx, LANG selectedLang) {
        if (selectedLang == LANG.ENGLISH) {
            // Change layout to en
            Locale mLocale = new Locale("en");
            Locale.setDefault(mLocale);
            Configuration config = ctx.getResources().getConfiguration();

            if (!config.locale.equals(mLocale)) {
                config.locale = mLocale;
                ctx.getResources().updateConfiguration(config, null);
            }
//            SharedPrefCustom.saveLocalization(ctx, english);
        } else {
            // Change layout to ar
            Locale mLocale = new Locale("ar");
            Locale.setDefault(mLocale);
            Configuration config = ctx.getResources().getConfiguration();
            if (!config.locale.equals(mLocale)) {
                config.locale = mLocale;
                ctx.getResources().updateConfiguration(config, null);
            }
//            SharedPrefCustom.saveLocalization(ctx, arabic);
        }
    }
//==========================================================================

    public void init() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList = (ListView) findViewById(R.id.adapter);
        drawerListLeft = (ListView) findViewById(R.id.adapterLeft);
        menuIV = (ImageView) findViewById(R.id.menuEn);
        notificationIV = (ImageView) findViewById(R.id.notification);
        menuIVAr = (ImageView) findViewById(R.id.menuAr);
        notificationIVAr = (ImageView) findViewById(R.id.notificationAr);
        linearLayout = (LinearLayout) findViewById(R.id.mainLayout);
        navigationViewLeft = (NavigationView) findViewById(R.id.nav_left_view);
        navigationViewRight = (NavigationView) findViewById(R.id.nav_right_view);
        commissionBtn = (Button) findViewById(R.id.commission);
        notifyLeftTV = (TextView) findViewById(R.id.notifyLeft);
        notifyRightTV = (TextView) findViewById(R.id.notifyRight);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        menuIV.bringToFront();

        notificationIV.bringToFront();
        menuIVAr.bringToFront();
        notificationIVAr.bringToFront();
        notifyLeftTV.bringToFront();
        notifyRightTV.bringToFront();

    }

    @Override
    public boolean isFinishing() {

        return super.isFinishing();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences1 = getSharedPreferences("userData", MODE_PRIVATE);
        final String driverId = sharedPreferences1.getString("id", "");
        mDatabase.child("driver").child(driverId).child("block").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot==null)
                    return;
                if ((Boolean) dataSnapshot.getValue() == true){

                    AlertDialog alertDialog = new AlertDialog.Builder(Home.this).create();
                    alertDialog.setMessage(getString(R.string.blok_msg) );
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.CommissionPayment),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    final Intent mainIntent = new Intent(Home.this, Commission.class);
                                    startActivity(mainIntent);
                                }
                            });
                    alertDialog.setCancelable(false);
                    alertDialog.show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
