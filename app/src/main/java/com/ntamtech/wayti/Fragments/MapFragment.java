package com.ntamtech.wayti.Fragments;


import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.wayti.LatLngInterpolator;
import com.ntamtech.wayti.MarkerAnimation;
import com.ntamtech.wayti.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;

import Bean.User;
import Util.DirectionsJSONParser;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {

    GoogleMap map;
    private LocationManager locationManager;
    private LocationListener listener;
    private DatabaseReference mDatabase;
    Marker markerAya;
    int flag = 0;
    Marker userMarker;
    String key;

    Double driverLng;
    Double driverLat;

    int markerIcon;


    Button myLocation;
    PolylineOptions lineOptions = null;
    ProgressBar progressbar;

    public MapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_map, container, false);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        myLocation = (Button)v.findViewById(R.id.myLocation);
        progressbar= (ProgressBar) v.findViewById(R.id.progressbar);
        SharedPreferences preferences = getActivity().getSharedPreferences("userData", MODE_PRIVATE);
        final String userId = preferences.getString("id", "");
        final boolean typeOfWater = preferences.getBoolean("typeOfWater", true);
        if (typeOfWater)
        {
            markerIcon = R.mipmap.car_icon;
        }else {
            markerIcon = R.mipmap.yellow_car;
        }

        myLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(driverLat, driverLng)));
                }catch (Exception e ){

                }
            }
        });

        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                LatLng pp = new LatLng(location.getLatitude(), location.getLongitude());
                if (flag == 0) {
                    if(map!=null) {
                        map.clear();
                        markerAya = map.addMarker(new MarkerOptions()
                                .position(new LatLng(location.getLatitude(), location.getLongitude()))
                                .icon(BitmapDescriptorFactory.fromResource(markerIcon)));

                        mDatabase.child("driver").child(userId).child("location_latitude").setValue(location.getLatitude());
                        mDatabase.child("driver").child(userId).child("location_longitude").setValue(location.getLongitude());
                        driverLat = location.getLatitude();
                        driverLng = location.getLongitude();

                        flag = 1;
                    }
                }

                mDatabase.child("driver").child(userId).child("location_latitude").setValue(location.getLatitude());
                mDatabase.child("driver").child(userId).child("location_longitude").setValue(location.getLongitude());
                driverLat = location.getLatitude();
                driverLng = location.getLongitude();

                if (getActivity()!=null){
                    SharedPreferences.Editor editor = getActivity().getSharedPreferences("userData", MODE_PRIVATE).edit();
                    editor.putString("lat", String.valueOf(location.getLatitude()));
                    editor.putString("lng", String.valueOf(location.getLongitude()));
                    editor.commit();
                }
                if(map==null)
                    return;
                map.moveCamera(CameraUpdateFactory.newLatLng(pp));
                if (markerAya!=null){
                    MarkerAnimation.animateMarkerToICS(markerAya, new LatLng(location.getLatitude(), location.getLongitude()), new LatLngInterpolator.Spherical());
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                buildAlertMessageNoGps();
            }
        };
        configure_button();

        return v;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        configure_button();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map1);
        mapFragment.getMapAsync(this);
        if(FirebaseAuth.getInstance().getCurrentUser()!= null){
            SharedPreferences preferences = getActivity().getSharedPreferences("userData", MODE_PRIVATE);
            String token = preferences.getString("token","");
            String id= FirebaseAuth.getInstance().getCurrentUser().getUid();
            FirebaseDatabase.getInstance().getReference("driver")
                    .child(id).child("token").setValue(token);
        }
    }

    void configure_button() {
        // first check for permissions
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}, 10);
            }
            return;
        }
        // this code won'textView execute IF permissions are not allowed, because in the line above there is return statement.

        //noinspection MissingPermission
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, listener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0, listener);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        if(driverLat==null || driverLat ==0.0 || driverLng==null || driverLng ==0.0) {
            SharedPreferences preferences = getActivity().getSharedPreferences("userData", MODE_PRIVATE);
            final String user_latitude = preferences.getString("lat","");
            final String user_longitude = preferences.getString("lng","");
            if(user_latitude.trim().isEmpty() ||user_longitude.trim().isEmpty())
                return;
            driverLat = Double.parseDouble(user_latitude);
            driverLng = Double.parseDouble(user_longitude);

        }
        LatLng pp = new LatLng(driverLat, driverLng);
        /*driverLat =23.8859;
        driverLng = 45.0792;*/
        markerAya = googleMap.addMarker(new MarkerOptions().position(pp)
                .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), markerIcon))));
        map.moveCamera(CameraUpdateFactory.newLatLng(pp));


        SharedPreferences preferences = getActivity().getSharedPreferences("requestData", MODE_PRIVATE);
        final String user_latitude = preferences.getString("user_latitude", " ");
        final String user_longitude = preferences.getString("user_longitude", " ");
        key = preferences.getString("key", "");

        if (map != null && !user_latitude.equals(" ") && !user_longitude.equals(" ") && !key.equals(" ")) {

            userMarker = map.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(user_latitude), Double.parseDouble(user_longitude)))
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.marker)));
            userListener(key);

            LatLng origin = new LatLng(driverLat,driverLng);
            LatLng dest = new LatLng(Double.parseDouble(user_latitude),Double.parseDouble(user_longitude));

            // Getting URL to the Google Directions API
            String url = getDirectionsUrl(origin, dest);

            DownloadTask downloadTask = new DownloadTask();

            // Start downloading json data from Google Directions API
            downloadTask.execute(url);

        }
    }
//===============================================

    private void buildAlertMessageNoGps(){

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                        Toast.makeText(getContext(), "cancel gps", Toast.LENGTH_SHORT).show();


                    }
                });
        // final AlertDialog alert = builder.create();
        builder.show();

    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences preferences = getActivity().getSharedPreferences("requestData", MODE_PRIVATE);
        final String user_latitude = preferences.getString("user_latitude", " ");
        final String user_longitude = preferences.getString("user_longitude", " ");
        key = preferences.getString("key", "");

        if (map != null && !user_latitude.equals(" ") && !user_longitude.equals(" ") && !key.equals(" ")) {

            userMarker = map.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(user_latitude), Double.parseDouble(user_longitude)))
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.marker)));
            userListener(key);

            LatLng origin = new LatLng(driverLat,driverLng);
            LatLng dest = new LatLng(Double.parseDouble(user_latitude),Double.parseDouble(user_longitude));

            // Getting URL to the Google Directions API
            String url = getDirectionsUrl(origin, dest);

            DownloadTask downloadTask = new DownloadTask();

            // Start downloading json data from Google Directions API
            downloadTask.execute(url);

        }

    }

    //==============================

    public void userListener(String key){

        mDatabase.child("request").child("accepted").child(key).child("status").addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {

                    if (dataSnapshot.getValue().toString().equals("1")) {

                        SharedPreferences.Editor editor = getActivity().getSharedPreferences("requestData", MODE_PRIVATE).edit();
                        editor.putString("user_latitude"," ");
                        editor.putString("user_longitude"," ");
                        editor.putString("key", "");
                        editor.commit();

                        if (userMarker!=null){
                            userMarker.remove();
                            userMarker = null;
                        }

                        alertMsg(getString(R.string.trip_successful));

                    }else {

                    }
                } else {
                    if(getActivity()==null)
                        return;
                    SharedPreferences.Editor editor = getActivity().getSharedPreferences("requestData", MODE_PRIVATE).edit();
                    editor.putString("user_latitude"," ");
                    editor.putString("user_longitude"," ");
                    editor.putString("key", "");
                    editor.commit();
                    if (userMarker != null) {
                        userMarker.remove();
                        userMarker = null;
                    }

                    alertMsg(getString(R.string.cancel_trip));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //==================================================================


    public void alertMsg(final String msg){
        final AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_DeviceDefault_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getContext());
        }
        builder.setTitle(msg)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        if(msg.equals(getString(R.string.trip_successful))){
                            map.clear();
                            LatLng pp = new LatLng(driverLat, driverLng);
                            markerAya = map.addMarker(new MarkerOptions().position(pp)
                                    .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), markerIcon))));
                            map.moveCamera(CameraUpdateFactory.newLatLng(pp));
                        }
                        dialog.dismiss();
                    }
                })
                .show();
    }
    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();


            parserTask.execute(result);

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(locationManager!=null && listener!=null)
            locationManager.removeUpdates(listener);
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points = null;

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(12);
                lineOptions.color(Color.RED);
                lineOptions.geodesic(true);
                progressbar.setVisibility(View.GONE);

            }

// Drawing polyline in the Google Map for the i-th route
            if(lineOptions !=null)
            map.addPolyline(lineOptions);
        }
    }
    }


