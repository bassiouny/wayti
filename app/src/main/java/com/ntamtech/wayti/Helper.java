package com.ntamtech.wayti;

import android.app.Activity;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by Ayaom on 5/16/2017.
 */

public class Helper {


    public void hideStatusBar(Activity activity){

        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

}
