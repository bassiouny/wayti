package com.ntamtech.wayti;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.wayti.Activities.Activation;
import com.ntamtech.wayti.Activities.ForgetPasswordActivity;
import com.ntamtech.wayti.Activities.Home;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import Bean.User;
import Util.Utility;
import config.SaveSharedPreference;


public class Login extends AppCompatActivity {

    TextView forgetPasswordTV;
    TextView registerTV;
    Button loginBtn;
    EditText phoneET, passwordET;
    private CoordinatorLayout coordinatorLayout;
    private FirebaseAuth auth;
    private DatabaseReference mDatabase;
    private ProgressDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (!SaveSharedPreference.getUserName(Login.this).equals("")) {
            Intent intent = new Intent(getApplicationContext(), Home.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {

            Log.i("sssss", SaveSharedPreference.getUserName(Login.this) + "");
        }
        intail();
        events();

    }

    public void intail() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        forgetPasswordTV = (TextView) findViewById(R.id.forgetPass);
        registerTV = (TextView) findViewById(R.id.register);
        loginBtn = (Button) findViewById(R.id.login);
        phoneET = (EditText) findViewById(R.id.phone);
        passwordET = (EditText) findViewById(R.id.password);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        auth = FirebaseAuth.getInstance();
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);

//        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
//        final String userId = preferences.getString("id", "");
//
//        if (!Utility.isNotNull(userId)){
//            startActivity(new Intent(Login.this, Home.class));
//        }


//        mAuthListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                FirebaseUser user = firebaseAuth.getCurrentUser();
//                if (user != null) {
//                    // User is signed in
//                    startActivity(new Intent(Login.this, Home.class));
//                } else {
//                    // User is signed out
//                    //  startActivity(new Intent(Login.this, Login.class));
//                }
//                // ...
//            }
//        };
    }

    public void events() {


        forgetPasswordTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this,ForgetPasswordActivity.class));
            }
        });

        registerTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent mainIntent = new Intent(Login.this, Registeration.class);
                startActivity(mainIntent);
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
                loginUser(phoneET.getText().toString(), passwordET.getText().toString());
            }
        });
    }

    private void loginUser(String phone, final String password) {

        if (phone.length() == 9) {
            if (Utility.isNotNull(phone) && Utility.isNotNull(password)) {
                phone = phone + "@gmail.com";
                auth.signInWithEmailAndPassword(phone, password)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull final Task<AuthResult> task) {
                                if (!task.isSuccessful()) {
                                    if (password.length() < 6) {
                                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "password length must be over 6", Snackbar.LENGTH_SHORT);
                                        snackbar.show();
                                    }
                                    dialog.hide();

                                } else {
                                    mDatabase.getRoot().child("driver").addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {

                                            if (dataSnapshot.hasChild(task.getResult().getUser().getUid())) {


                                                mDatabase.getRoot().child("driver").child(task.getResult().getUser().getUid()).addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                                        if (!dataSnapshot.getValue(User.class).getBlock()) {
                                                            String id = task.getResult().getUser().getUid();
                                                            String name = dataSnapshot.getValue(User.class).getName();
                                                            String address_latitude = dataSnapshot.getValue(User.class).getAddress_latitude();
                                                            String address_longitude = dataSnapshot.getValue(User.class).getAddress_longitude();
                                                            String car_img1 = dataSnapshot.getValue(User.class).getCar_img1();
                                                            String car_img2 = dataSnapshot.getValue(User.class).getCar_img2();
                                                            double location_latitude = dataSnapshot.getValue(User.class).getLocation_latitude();
                                                            double location_longitude = dataSnapshot.getValue(User.class).getLocation_longitude();
                                                            String mobile = dataSnapshot.getValue(User.class).getMobile();
                                                            String photo = dataSnapshot.getValue(User.class).getPhoto();
                                                            boolean isDrinkable = dataSnapshot.getValue(User.class).getIsDrinkable();
                                                            int total_no_of_rate = dataSnapshot.getValue(User.class).getTotal_no_of_rate();
                                                            int total_rate = dataSnapshot.getValue(User.class).getTotal_rate();

                                                            int weight = dataSnapshot.getValue(User.class).getWeight();
                                                            String city = dataSnapshot.getValue(User.class).getCity();
                                                            double price = dataSnapshot.getValue(User.class).getPrice();                                                          SaveSharedPreference.setUserName(getApplicationContext(), mobile);
                                                            mDatabase.child("driver").child(id).child("exit").setValue(false);
                                                            SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                                                            editor.putString("id", id);
                                                            editor.putString("name", name);
                                                            editor.putString("phone", mobile);
                                                            editor.putString("carImg1", car_img1);
                                                            editor.putString("password", password);
                                                            editor.putString("carImg2", car_img2);

                                                            editor.putString("lat", String.valueOf(location_latitude));
                                                            editor.putString("lng", String.valueOf(location_longitude));


                                                            editor.putString("profileImg", photo);
                                                            editor.putBoolean("typeOfWater", isDrinkable);
                                                            editor.putInt("weight", weight);
                                                            editor.putString("city", city);
                                                            editor.putFloat("price", (float)price);
                                                            editor.commit();
                                                            dialog.hide();
                                                            boolean isActive = dataSnapshot.getValue(User.class).getIsActive();
                                                            if (!SaveSharedPreference.getUserName(Login.this).equals("")) {
                                                                if (isActive == true) {
                                                                    startActivity(new Intent(Login.this, Home.class));
                                                                    finish();
                                                                } else {
                                                                    startActivity(new Intent(Login.this, Activation.class));
                                                                    finish();
                                                                }
                                                            }
                                                        } else {
                                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.blok_msg), Toast.LENGTH_SHORT).show();
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {
                                                        dialog.hide();
                                                    }
                                                });
                                            } else {
                                                dialog.hide();
                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.userNotFound), Toast.LENGTH_SHORT).show();

                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            dialog.hide();
                                        }
                                    });
                                }
                            }
                        });
            } else {
                dialog.hide();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.fillData), Toast.LENGTH_SHORT).show();
            }
        } else {
            dialog.hide();
            final AlertDialog alertDialog = new AlertDialog.Builder(Login.this).create();
            alertDialog.setMessage(getString(R.string.format_num));
            alertDialog.setCancelable(false);
            alertDialog.setButton(Dialog.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        auth.addAuthStateListener(mAuthListener);
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        if (mAuthListener != null) {
//            auth.removeAuthStateListener(mAuthListener);
//        }
//    }


    public static String makePostRequest(String stringUrl, String payload, Context context) throws IOException {
        Log.e("makePostRequest: ",payload );
        URL url = new URL(stringUrl);
        HttpURLConnection uc = (HttpURLConnection) url.openConnection();
        String line;
        StringBuffer jsonString = new StringBuffer();
        uc.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
        uc.setRequestProperty( "charset", "utf-8");
        uc.setRequestMethod("POST");
        uc.setDoInput(true);
        uc.setInstanceFollowRedirects(false);
        uc.connect();
        OutputStreamWriter writer = new OutputStreamWriter(uc.getOutputStream(), "UTF-8");
        writer.write(payload);
        writer.close();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            while ((line = br.readLine()) != null) {
                jsonString.append(line);
            }
            br.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        uc.disconnect();
        JSONObject jObj ;
        String resutl;
        try {
            jObj = new JSONObject(jsonString.toString());
            resutl = jObj.getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
            resutl="Sorry Message Not Send";
        }
        return resutl;
    }
}
