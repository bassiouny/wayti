package com.ntamtech.wayti.Singleton;

/**
 * Created by Ayaom on 6/21/2017.
 */

public class singleton {

    private static singleton myObj;

    /**
     * Create private constructor
     */
    private singleton() {

    }

    /**
     * Create a static method to get instance.
     */
    public static singleton getInstance() {
        if (myObj == null) {
            myObj = new singleton();
        }
        return myObj;
    }

}
