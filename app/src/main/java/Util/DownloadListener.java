package Util;

/**
 * Created by ahmed on 26/09/17.
 */

public interface DownloadListener {
    void onDownloadFinish();
}
