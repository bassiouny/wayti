package Util;


import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ProgressBar;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by ahmed on 26/09/17.
 */

public class DownloadUrl {
    private static DownloadUrl internetFilesOperations;
    private Context context;

    public static DownloadUrl getInstance(Context context) {
        if (internetFilesOperations != null)
            return internetFilesOperations;
        else {
            return new DownloadUrl(context);
        }
    }
    private DownloadUrl(Context context) {
        this.context = context;

    }
    public void downloadUrlWithProgress( String downloadUrl,String filePathDevice,DownloadListener onDownloadFinish) {

        new DownloadFileFromURL(downloadUrl, onDownloadFinish,filePathDevice).execute(downloadUrl);
    }


    class DownloadFileFromURL extends AsyncTask<String, String, String> {


        DownloadListener onDownloadFinish;
        String downloadUrl;
        String filePathDevice;
        public DownloadFileFromURL( String downloadUrl, DownloadListener onDownloadFinish,String filePathDevice) {

            this.downloadUrl = downloadUrl;
            this.onDownloadFinish = onDownloadFinish;
            this.filePathDevice=filePathDevice;


        }

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            URL url;
            try {
                url = new URL(downloadUrl);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                //this will be used to write the downloaded data into the file we created
                FileOutputStream output = new FileOutputStream(filePathDevice);


                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();


            } catch (Exception e) {
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage

        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            onDownloadFinish.onDownloadFinish();
        }
    }
}
