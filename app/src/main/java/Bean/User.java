package Bean;

import java.io.Serializable;

/**
 * Created by Ayaomar on 6/4/2017.
 */

public class User{


    String id;
    String name;
    String address_latitude;
    String address_longitude;
    String car_img1;
    String car_img2;
    double location_latitude;
    double location_longitude;
    String mobile;
    String photo;
    int status;
    int total_no_of_rate;
    int total_rate;
    boolean isDrinkable;
    int weight;
    String city;
    boolean isActive;
    boolean block;
    String password;
    private double price;


    public User() {
    }


    public User(String id, String name, String address_latitude, String address_longitude, String car_img1, String car_img2, double location_latitude, double location_longitude, String mobile, String photo, int status, int total_no_of_rate, int total_rate, boolean isDrinkable, int weight, String city
    ,boolean isActive ,boolean block) {
        this.id = id;
        this.name = name;
        this.address_latitude = address_latitude;
        this.address_longitude = address_longitude;
        this.car_img1 = car_img1;
        this.car_img2 = car_img2;
        this.location_latitude = location_latitude;
        this.location_longitude = location_longitude;
        this.mobile = mobile;
        this.photo = photo;
        this.status = status;
        this.total_no_of_rate = total_no_of_rate;
        this.total_rate = total_rate;
        this.isDrinkable = isDrinkable;
        this.weight = weight;
        this.city = city;
        this.isActive = isActive;
        this.block = block;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress_latitude() {
        return address_latitude;
    }

    public void setAddress_latitude(String address_latitude) {
        this.address_latitude = address_latitude;
    }

    public String getAddress_longitude() {
        return address_longitude;
    }

    public void setAddress_longitude(String address_longitude) {
        this.address_longitude = address_longitude;
    }

    public String getCar_img1() {
        return car_img1;
    }

    public void setCar_img1(String car_img1) {
        this.car_img1 = car_img1;
    }

    public String getCar_img2() {
        return car_img2;
    }

    public void setCar_img2(String car_img2) {
        this.car_img2 = car_img2;
    }

    public double getLocation_latitude() {
        return location_latitude;
    }

    public void setLocation_latitude(double location_latitude) {
        this.location_latitude = location_latitude;
    }

    public double getLocation_longitude() {
        return location_longitude;
    }

    public void setLocation_longitude(double location_longitude) {
        this.location_longitude = location_longitude;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTotal_no_of_rate() {
        return total_no_of_rate;
    }

    public void setTotal_no_of_rate(int total_no_of_rate) {
        this.total_no_of_rate = total_no_of_rate;
    }

    public int getTotal_rate() {
        return total_rate;
    }

    public void setTotal_rate(int total_rate) {
        this.total_rate = total_rate;
    }

    public boolean getIsDrinkable() {
        return isDrinkable;
    }

    public void setDrinkable(boolean drinkable) {
        isDrinkable = drinkable;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean active) {
        isActive = active;
    }

    public boolean getBlock() {
        return block;
    }

    public void setBlock(boolean block) {
        this.block = block;
    }

    public double getPrice() {
        if(price == 0)
            price=1;
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPassword() {
        if(password==null)
            password="";
        return password;
    }
}
