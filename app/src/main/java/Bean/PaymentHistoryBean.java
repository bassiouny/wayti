package Bean;

/**
 * Created by Ayaom on 6/21/2017.
 */

public class PaymentHistoryBean {

    String bank_name;
    String date_time;
    String payment_no;
    String price;

    public PaymentHistoryBean() {
    }

    public PaymentHistoryBean(String bank_name, String date_time, String payment_no, String price) {
        this.bank_name = bank_name;
        this.date_time = date_time;
        this.payment_no = payment_no;
        this.price = price;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getPayment_no() {
        return payment_no;
    }

    public void setPayment_no(String payment_no) {
        this.payment_no = payment_no;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
