package Bean;

/**
 * Created by Ayaom on 6/15/2017.
 */

public class historyBean {
    String date;
    String driver_name;
    String driver_phone;
    String driver_photo;
    String status;
    String user_phone;
    String user_photo;
    String username;
    double user_latitude;
    double user_longitude;

    public historyBean() {
    }

    public historyBean(String username, String user_phone, String date, String user_photo ,double user_latitude , double user_longitude ) {
        this.username = username;
        this.user_phone = user_phone;
        this.date = date;
        this.user_photo = user_photo;
        this.user_latitude = user_latitude;
        this.user_longitude = user_longitude;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getDriver_phone() {
        return driver_phone;
    }

    public void setDriver_phone(String driver_phone) {
        this.driver_phone = driver_phone;
    }

    public String getDriver_photo() {
        return driver_photo;
    }

    public void setDriver_photo(String driver_photo) {
        this.driver_photo = driver_photo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getUser_latitude() {
        return user_latitude;
    }

    public void setUser_latitude(double user_latitude) {
        this.user_latitude = user_latitude;
    }

    public double getUser_longitude() {
        return user_longitude;
    }

    public void setUser_longitude(double user_longitude) {
        this.user_longitude = user_longitude;
    }
}
