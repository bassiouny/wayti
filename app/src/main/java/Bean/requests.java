package Bean;

/**
 * Created by Ayaom on 6/20/2017.
 */

public class requests {

    String toId;
    String username;
    String user_photo;
    String user_phone;
    int status;
    double user_latitude;
    double user_longitude;

    public requests(String toId,String username, String user_photo ,int status,String user_phone ,  double user_latitude ,   double user_longitude ) {
        this.username = username;
        this.user_photo = user_photo;
        this.status = status;
        this.toId = toId;
        this.user_phone = user_phone;
        this.user_latitude = user_latitude;
        this.user_longitude = user_longitude;
    }

    public requests(){
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public double getUser_latitude() {
        return user_latitude;
    }

    public void setUser_latitude(double user_latitude) {
        this.user_latitude = user_latitude;
    }

    public double getUser_longitude() {
        return user_longitude;
    }

    public void setUser_longitude(double user_longitude) {
        this.user_longitude = user_longitude;
    }
}
